//import { BrowserRouter as  Redirect, Route, Switch } from 'react-router-dom';
import React, { useEffect }  from 'react';

import {  Route, Switch } from 'react-router-dom';

import { useDispatch, useSelector } from "react-redux";


import { useRouter } from './useRouter';


import ProtectedRoute from './ProtectedRoute'

import ThemeToggle from '../components/ThemeToggle';
import ToastAlert from '../components/ToastAlert';

import cx from 'classnames';

import { useTransition, animated } from 'react-spring'

import Login from '../pages/Login';
import SignUp from '../pages/SignUp';
import ForgotPass from '../pages/ForgotPass';

import Home from '../pages/Home';
import Wallet from '../pages/Wallet';
import Exchange from '../pages/Exchange/';
import Transactions from '../pages/Transactions';

import Settings from '../pages/Settings/';
import Support from '../pages/Support';

import ThemeTest from '../pages/ThemeTest';


const App = (props) => {


    const { location } = useRouter()
    const transitions = useTransition(location, location => location.pathname, {
      from: { opacity: 0 },
      enter: { opacity: 1 },
      leave: { opacity: 0 },
      config: { duration: 250 }
    })

    const { theme } = useSelector(state => state.Theme);

    const dispatch = useDispatch();


    return (

        <div 
          className={cx(
            "app app-theme-" + theme,
            { 'bg-color-black-pearl text-white': theme === 'dark' },
            { 'bg-color-alabaster text-color-outer-space': theme === 'light' },
          )}
        >


          <ThemeToggle/>

              {
                transitions.map(({ item, props, key }) => (
                  <animated.div key={key} style={props} className="pages">
                    <Switch location={item}>
                      <Route exact path="/" component={Login}/>
                      <Route exact path="/login" component={Login}/>
                      <Route exact path="/signup" component={SignUp}/>
                      <Route exact path="/forgotPassword" component={ForgotPass}/>
                      
                      <ProtectedRoute exact path="/home" component={Home}/>
                      <ProtectedRoute exact path="/wallet" component={Wallet}/>
                      <ProtectedRoute exact path="/exchange" component={Exchange}/>
                      <ProtectedRoute exact path="/transactions" component={Transactions}/>

                      <ProtectedRoute exact path="/settings" component={Settings}/>
                      <ProtectedRoute exact path="/support" component={Support}/>

                      <ProtectedRoute exact path="/themetest" component={ThemeTest}/>
                    </Switch>
                  </animated.div>))
              }
          <ToastAlert/>


        </div>
    )
};

export default App;