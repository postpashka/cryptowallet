import React from 'react';
import Sidebar from '../components/Sidebar';

import {
    Col, 
    Row,
} from 'reactstrap';



const SidebarLayout = (props) => {

    return (
        <Row className="h-100 m-0">
          <Col sm={3} className="h-100 mh-100 p-0">
            <Sidebar {...props}/>
          </Col>
          <Col sm={9} className="h-100 mh-100 p-0">
            <div className="d-flex flex-column h-100 mh-100 py-4 px-5">
              {props.children}
            </div>
          </Col>
        </Row>
    )
};

export default SidebarLayout;