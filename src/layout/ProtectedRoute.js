import { Route, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import React from 'react';

const ProtectedRoute = ({ component: Component, ...rest }) => {
  return (
    <Route {...rest} render={
      props => {
        if (rest.loggedIn) {
          return <Component {...rest} {...props} />
        } else {
          return <Redirect to={
            {
              pathname: '/login',
              state: {
                from: props.location
              }
            }
          } />
        }
      }
    } />
  )
}

const mapStateToProps = state => ({
    loggedIn: state.Auth.loggedIn,
});

export default connect(mapStateToProps, null)(ProtectedRoute);
