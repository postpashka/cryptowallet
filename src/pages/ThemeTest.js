import React, { Fragment, useState, useRef } from "react";




const initialList = ["Paypal", "CCAvenue", "Razorpay", "Stripe", "Google Pay"];

const DragDrop = () => {
  const draggingItem = useRef();
  const dragOverItem = useRef();

  const [paymentGateWayList, setPaymentGateWayList] = useState(initialList);

  const handleDragStart = (e, position) => {
    draggingItem.current = position;
  };

  const handleDragEnter = (e, position) => {
    dragOverItem.current = position;
    const updatedList = [...paymentGateWayList];
    const draggingItemContent = updatedList[draggingItem.current];
    updatedList.splice(draggingItem.current, 1);
    updatedList.splice(dragOverItem.current, 0, draggingItemContent);
    draggingItem.current = dragOverItem.current;
    dragOverItem.current = null;
    // Update PG list in database
    setPaymentGateWayList(updatedList);
  };

  const onClickDiv = () => {
    alert('yes');

  }

  return (
    <Fragment>
      {paymentGateWayList &&
        paymentGateWayList.map((item, index) => (
          <div
            className="border m-1"
            onDragEnter={(e) => handleDragEnter(e, index)}
            onDragStart={(e) => handleDragStart(e, index)}
            key={index}
            draggable
            onClick={onClickDiv}
          >
          	<div className="icon">icon</div>
            {item}
          </div>
        ))}
    </Fragment>
  );
};


const Home = (props) => {


	return (
		<div className="page">
			test
			<DragDrop/>
		</div>
	);
}

export default Home;

