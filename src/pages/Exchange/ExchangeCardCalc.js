import React, { Fragment } from "react";

import { useSelector } from "react-redux";

import cx from 'classnames';

import {
		Media,
} from 'reactstrap';




const ExchangeCardCalc = (props) => {

	const { theme } = useSelector(state => state.Theme);


	if (!props.currency){
		return null
	} else {

		return (
			<Fragment>

				<h5
					className="py-5"
				>
					{props.title}
				</h5>
				<Media 
					className="text-white align-items-center"
				>
					<Media left middle href="#" className="pr-3">
						<img width="50px" height="50px" src={props.currency.img} alt="Generic placeholder" />
					</Media>
					<Media 
						body 
						className={cx(
							'small',
							{ 'text-white': theme === 'dark' },
							{ 'text-color-outer-space': theme === 'light' },
						)}
					>
						<Media 
							className="pb-2"
							//style={{color: color}}
						>
							<div className="d-flex align-items-center w-50">
								<h6>
									{props.currency.name}
								</h6>
								<div 
									className="d-flex align-items-center ml-auto"

								>
									<span className="px-3"> 0.00 </span>
									<span> {props.currency.name} </span>
								</div>
							</div>
						</Media>
						<div className="d-flex align-items-center">
							<div className="d-flex align-items-center w-50">
								<span>
									{props.currency.name}
								</span>
								<div className="d-flex align-items-center ml-auto">
									<span className="px-3"> 0.00 </span>
									<span> USD </span>
								</div>
							</div>
						</div>
					</Media>
				</Media>
			</Fragment>
		)

	}
}


export default ExchangeCardCalc;

