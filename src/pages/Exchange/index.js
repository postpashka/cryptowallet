import React, { useState } from "react";

import SidebarLayout from '../../layout/SidebarLayout';
import { useSelector } from "react-redux";

import cx from 'classnames';


import ExchangeCard from './ExchangeCard';
import ExchangeCardCalc from './ExchangeCardCalc';

import {
		Col, 
		Row,
		Button
} from 'reactstrap';

import {ReactComponent as ArrowsUpDown} from '../../assets/icons/arrows-up-down.svg';


const ExchangeCards = (props) => {

	const { theme } = useSelector(state => state.Theme);


	const [ currencies, setCurrencies ] = useState(  [] );




	function setExchangeCurrencies(index, curr) {

		let tmpArr = [...currencies]

		tmpArr[index] = curr;

		setCurrencies( tmpArr )

	}

	function switchCards(index) {

		let tmpArr = [...currencies]

		tmpArr.reverse();

		setCurrencies( tmpArr )

	}

	return (
		<div className="home-assets flex-grow-1 h-0">
			<Row className="h-100 overflow-hidden m-0">
				<Col sm={12} className="h-100 pl-0 pr-5">
					<div className="d-flex flex-column h-100 mh-100">
						<div className="border-bottom border-dark mb-4 pb-3">
							<h4>Exchange</h4>
							<div className="d-flex justify-content-between align-items-center">
								<h6 
									className={cx(
										'py-1',
										{ 'text-color-oslo-gray': theme === 'dark' },
										{ 'text-color-bitter': theme === 'light' },
									)}
								>
									Use below interface to exchange two assets with each other.
								</h6>
							</div>
						</div>
						<div className="mb-4 pb-3">
							<Row>
								<Col sm={7}>


									<ExchangeCard currency={currencies} setCurrency={(curr)=>setExchangeCurrencies(0, curr)} key="0" index="0"/>

									<div
										style={{'cursor': 'pointer'}}
										className="d-flex"
									>
										<div
											onClick={switchCards}

											className={cx(
												'p-3 d-inline-flex mx-auto my-n3 rounded-circle',
												{ 'bg-color-picton-blue': theme === 'dark' },
												{ 'bg-color-jelly-bean': theme === 'light' },
											)}
										>
										<ArrowsUpDown
											className={cx(
												{ 'icon-black': theme === 'dark' },
												{ 'icon-white': theme === 'light' },
											)}
										/>
										</div>
									</div>

									<ExchangeCard currency={currencies}  setCurrency={(curr)=>setExchangeCurrencies(1, curr)} key="1" index="1"/>

								</Col>
								<Col sm={5}>

									<div className="border-bottom border-dark mb-4 pb-3">

										<h5>Exchange Calculations</h5>
										<div className="d-flex justify-content-between align-items-center">
											<p 
												className={cx(
													'py-1',
													{ 'text-color-oslo-gray': theme === 'dark' },
													{ 'text-color-bitter': theme === 'light' },
												)}
											>
												Use below interface to exchange two assets with each other.
											</p>
										</div>

										<ExchangeCardCalc currency={currencies[0]} title="Your Are Exchanging"/>

										<ExchangeCardCalc currency={currencies[1]}  title="Your Are Exchanging"/>

									</div>

									<Row>
										<Col sm={6} className="align-items-center d-flex">
											<Button
												color="primary"
												className="btn-lg h5 px-5 btn-block"
											>
												Exchange
											</Button>
										</Col>
										<Col sm={6} className="align-items-center d-flex">
											<span className="px-2">1 DASH = 0.00102597 BTC</span>
										</Col>
									</Row>

								</Col>
							</Row>
						</div>


					</div>
				</Col>
			</Row>
		</div>
	)
}


const Exchange = (props) => {

	return (
		<div className="page login-page">
			<SidebarLayout {...props}>
				<ExchangeCards/>
			</SidebarLayout>
		</div>
	);
}

export default Exchange;

