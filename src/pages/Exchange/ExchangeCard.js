import React, { Fragment, useState } from "react";
import { useSelector } from "react-redux";



import {
		Button,
		Input,
		Media,
		Row,
		Col, 
		FormGroup,
		Label,
} from 'reactstrap';



import Select, { components } from "react-select";


import cx from 'classnames';



import currencyList from '../../components/CurrencyList';



const options = Object.keys(currencyList).map(key => Object.assign({ key }, currencyList[key])); 


options[0].isdisabled = true;
options[1].isdisabled = true;
options[2].isdisabled = true;


const customStyles = {
	option: (provided, state) => ({
		...provided,
		padding: '8px 16px',
		backgroundColor: 'transparent',
		cursor: 'pointer',
	}),
	control: (provided, state) => ({
		height: 74,
		display: 'flex',
		border: '1px solid #56CCF2',
		borderRadius: '10px',
		padding: '8px 16px',
		cursor: 'pointer',
		backgroundColor:  state.selectProps.theme === 'dark' ? '#0E2D37' : '#FFFFFF',
	}),
	indicatorSeparator: () => ({
		display: 'none',
	}),


	valueContainer: () => ({
		padding: '0',
		display: 'flex',
		flex: '1',
		position: 'relative',
		overflow: 'hidden',
		boxSizing: 'borderBox',
	}),

	menu: (provided, state) => ({
		position: 'absolute',
		width: '100%',
		color: state.selectProps.theme === 'dark' ? '#FFFFFF' : '#000000',
		backgroundColor:  state.selectProps.theme === 'dark' ? '#08232C' : '#FFFFFF',
		zIndex: '10'
	}),

	singleValue: (provided, state) => ({
		color: state.selectProps.theme === 'dark' ? '#FFFFFF' : '#313942',
	})
}


const formatOptionLabel = ({ key, name, img, color, isdisabled }) => (

	<Media 
		className={cx(
				{ 'isdisabled': isdisabled },
		)}
	>
		<Media 
			left 
			middle 
			href="#" 
			className="mr-3"
		>
			<img width="50px" height="50px" src={img} alt="Generic placeholder" />
		</Media>
		<Media body>
			<Media 
				heading 
				style={{color: color}}
			>
				{name}
			</Media>
			{key}
		</Media>
	</Media>
);



const MenuList = (props) => {
	return (
		<Fragment>
			<components.MenuList 
				style={{overflow: 'hidden scroll'}}
				className="custom-scroll"
				{...props}
			>

				{props.children}

			</components.MenuList>
		</Fragment>
	);
};




const CustomControl = (props) => {

	const { theme } = useSelector(state => state.Theme);


	function onChange(value) {
		props.setCurrency(value)
	}

	return (


		<Select
			//defaultMenuIsOpen
			components={{ MenuList }}
			onChange={(value)=>onChange(value)}
			theme={theme}
			styles={customStyles}
			//defaultValue={options[0]}
			formatOptionLabel={formatOptionLabel}
			options={options}
			//isOptionDisabled={(option) => option.isdisabled}
			{...props}
		/>
	);
}





const ExchangeCard = (props) => {


	const { theme } = useSelector(state => state.Theme);


	const [ activeItem, setActiveItem ] = useState( 1 );

	function toggleBtn(index) {


	    if ( index === activeItem ) {
	        setActiveItem(0)
	    } else {
	        setActiveItem(index)
	    }

	}

	return (
		<div
			className={cx(
				"exchange-card py-4 px-4",
				{ 'bg-color-elephant text-white': theme === 'dark' },
				{ 'bg-white': theme === 'light' }
			)} 
			>	

			<div className="px-1">
				<CustomControl 
					setCurrency={props.setCurrency} 
					value={props.currency[props.index]}
					/>

				<Row className="mx-n2 my-4">
					<Col md={3} className="px-2">
						<FormGroup>
							<Label className="small" for="Balance">Your Balance</Label>
							<p className="py-2">3.0611162 DASH</p> 
						</FormGroup>
					</Col>
					<Col md={9} className="px-2">
						<FormGroup>
							<Label className="small" for="USD">Auto Amount</Label>
							<Row className="mx-n1">
								<Col sm={4} className="px-1">
								    <Button
								        outline
								        color="info"
								        onClick={()=>toggleBtn(1)}
								        className={cx(
								            'btn-block',
								            { 'btn-info text-white': activeItem === 1 }
								        )}
								        //color="info"
								    >
								    MAX
								  </Button>
								</Col>
								<Col sm={4} className="px-1">
								    <Button
								        outline
								        color="info"
								        onClick={()=>toggleBtn(2)}
								        className={cx(
								            'btn-block',
								            { 'btn-info text-white': activeItem === 2 }
								        )}
								        //outline
								        //color="info"
								    >
								    HALF
								  </Button>
								</Col>
								<Col sm={4} className="px-1">
								    <Button
								        outline
								        color="info"
								        onClick={()=>toggleBtn(3)}
								        className={cx(
								            'btn-block',
								            { 'btn-info text-white': activeItem === 3 }
								        )}
								    >
								    MIN
								  </Button>
								</Col>
							</Row>
						</FormGroup>
					</Col>
				</Row>
				<Row className="mx-n2 my-4">
					<Col md={3} className="px-2">
						<FormGroup>
							<Label className="small" for="Balance">Value</Label>
							<p className="py-2 m-1">$230.06</p> 
						</FormGroup>
					</Col>
					<Col md={9} className="px-2">
						<Row className="mx-n2">
							<Col md={6} className="px-2">
								<FormGroup>
									<Label className="small" for="BTC">BTC</Label>
									<Input 
										className={cx(
												"form-control rounded-lg",
												{ 'bg-color-nile-blue text-color-smalt-blue': theme === 'dark' },
												{ 'bg-color-mercury-3 text-color-bitter': theme === 'light' }
										)}
										type="email"
										name="BTC1"
										id="BTC1"
										placeholder="Enter amount in BTC"
										value="123"
									/>
								</FormGroup>
							</Col>
							<Col md={6} className="px-2">
								<FormGroup>
									<Label className="small" for="USD">USD</Label>
									<Input 
										className={cx(
												"form-control rounded-lg",
												{ 'bg-color-nile-blue text-color-smalt-blue': theme === 'dark' },
												{ 'bg-color-mercury-3 text-color-bitter': theme === 'light' }
										)}
										type="email"
										name="USD2"
										id="USD2"
										placeholder="Enter amount in USD"
										value="123"
									/>
								</FormGroup>
							</Col>
						</Row>

					</Col>
				</Row>

			</div>
		</div>
	);
}

export default ExchangeCard;

