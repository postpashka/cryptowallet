import React, { useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";

import { Link } from "react-router-dom";

// import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";

import {
    Button,
    Col, 
    Row,
    FormGroup,
    Label,
} from 'reactstrap';

import cx from 'classnames';

import signUp from '../assets/imgs/signup-bg.jpg';

import emailIcon from '../assets/icons/email.svg';
import passwordIcon from '../assets/icons/password.svg';
import nameIcon from '../assets/icons/user.svg';

import { authActions } from "../actions/auth";


import {ReactComponent as EyeIcon} from '../assets/icons/eye.svg';

import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';


const SignUp = (props) => {

    const { theme } = useSelector(state => state.Theme);

    const { registering } = useSelector(state => state.Registration);

    const [isPassVisible, setPassVisible] = useState(false);

    const [submitted, setSubmitted] = useState(false);

    const dispatch = useDispatch();
    
    return (
        <Formik
            initialValues={{
                name: '123',
                email: 'emailtesttool123@gmail.com',
                password: '123123',
                confirmPassword: '123123'
            }}
            validationSchema={Yup.object().shape({
                name: Yup.string()
                    .required('Name is required'),
                email: Yup.string()
                    .email('Email is invalid')
                    .required('Email is required'),
                password: Yup.string()
                    .min(6, 'Password must be at least 6 characters')
                    .required('Password is required'),
                confirmPassword: Yup.string()
                    .oneOf([Yup.ref('password'), null], 'Passwords must match')
                    .required('Confirm Password is required')
            })}
            onSubmit={ (fields, actions) => {
                setSubmitted(true);
                dispatch(authActions.register(fields));
                setSubmitted(false);
            }}
            render={({ errors, status, touched }) => (
                <div className="page signup-page">
                  {JSON.stringify(registering)}
                  <Row className="m-0 h-100">
                    <Col sm={6} className="p-0">
                      <div 
                        className="card-img-overlay"
                        style={{
                               background: `url(${signUp})`,
                               backgroundSize: 'cover'
                             }}
                      />
                      <div 
                        className="card-img-overlay bg-color-black-pearl opacity-5"
                      />
                    </Col>
                    <Col sm={6} 
                      className={cx(
                        "d-flex align-items-center justify-content-center p-0",
                        { 'bg-color-alabaster': theme === 'light' },
                      )}
                    >
                      <div className="d-flex flex-column w-50"> 
                        <h2 className="text-center mb-3 font-weight-bold">Sign Up</h2>
                        <h5
                          className={cx(
                            "text-center",
                            { 'text-color-river-bed': theme === 'dark' },
                            { 'text-color-lemon-grass': theme === 'light' },

                          )} 
                        >
                          Enter your Email and new Password
                        </h5>
                        <Form className="w-100 my-5">
                            <FormGroup className="mb-4">
                                <Label 
                                  className={cx(
                                    "small p-2",
                                    { 'text-color-cutty-shark': theme === 'dark' },
                                    { 'text-color-lemon-grass': theme === 'light' },
                                  )}
                                  for="name"
                                >
                                  Name
                                </Label>
                                <div className="position-relative">
                                  <img src={nameIcon} className="input-icon input-icon-left position-absolute p-3" alt="placeholder"/>
                                  <Field
                                      name="name"
                                      type="text"
                                      className={cx(
                                        "form-control form-control-lg pl-5 rounded-lg",
                                        { 'is-invalid': ( errors.name && touched.name ) },
                                        { 'bg-color-firefly-2 text-color-cutty-shark-2': theme === 'dark' },
                                        { 'bg-color-mercury-2 text-color-silver-chalice': theme === 'light' }
                                      )}
                                  />
                                  <ErrorMessage name="name" component="div" className="invalid-feedback" />
                                </div>
                            </FormGroup>

                            <FormGroup className="mb-4">
                                <Label
                                  className={cx(
                                    "small p-2",
                                    { 'text-color-cutty-shark': theme === 'dark' },
                                    { 'text-color-lemon-grass': theme === 'light' },
                                  )}
                                  for="email"
                                >
                                  Email
                                </Label>
                                <div className="position-relative">
                                  <img src={emailIcon} className="input-icon input-icon-left position-absolute p-3"  alt="placeholder"/>
                                  <Field
                                      type="email"
                                      className={cx(
                                        "form-control form-control-lg pl-5 rounded-lg",
                                        { 'is-invalid': ( errors.email && touched.email ) },
                                        { 'bg-color-firefly-2 text-color-cutty-shark-2': theme === 'dark' },
                                        { 'bg-color-mercury-2 text-color-silver-chalice': theme === 'light' }
                                      )}
                                      name="email"
                                  />
                                  <ErrorMessage name="email" component="div" className="invalid-feedback" />
                                </div>
                            </FormGroup>
                            <FormGroup className="position-relative mb-4">
                                <Label
                                  className={cx(
                                    "small p-2",
                                    { 'text-color-cutty-shark': theme === 'dark' },
                                    { 'text-color-lemon-grass': theme === 'light' },
                                  )}
                                  for="password"
                                >
                                  Password
                                </Label>
                                <div className="position-relative">
                                  <img src={passwordIcon} className="input-icon input-icon-left position-absolute p-3"  alt="placeholder"/>
                                  <Link
                                    href="#"
                                    onClick={()=>setPassVisible(!isPassVisible)}
                                  >

                                    <div className="input-icon input-icon-right position-absolute p-3">
                                      <EyeIcon 
                                        className={cx(
                                          { 'icon-gray': theme === 'light' },
                                          { 'icon-blue-light': theme === 'dark' }
                                        )}
                                      />
                                    </div>
                                  </Link>
                                  <Field
                                    type={!isPassVisible ? 'password' : 'text'}
                                    className={cx(
                                      "form-control form-control-lg pl-5 rounded-lg",
                                      { 'is-invalid': ( errors.password && touched.password ) },
                                      { 'bg-color-firefly-2 text-color-cutty-shark-2': theme === 'dark' },
                                      { 'bg-color-mercury-2 text-color-silver-chalice': theme === 'light' }
                                    )}
                                    name="password"
                                  />
                                  <ErrorMessage name="password" component="div" className="invalid-feedback" />
                                </div>
                            </FormGroup>
                            <FormGroup className="position-relative mb-4">
                                <Label
                                  for="confirmPassword"
                                  className={cx(
                                    "small p-2",
                                    { 'text-color-cutty-shark': theme === 'dark' },
                                    { 'text-color-lemon-grass': theme === 'light' },
                                  )}
                                >
                                  Confirm password
                                </Label>
                                <div className="position-relative">
                                  <img src={passwordIcon} className="input-icon input-icon-left position-absolute p-3"  alt="placeholder"/>
                                  <Link
                                    href="#"
                                    onClick={()=>setPassVisible(!isPassVisible)}
                                  >

                                    <div className="input-icon input-icon-right position-absolute p-3">
                                      <EyeIcon 
                                        className={cx(
                                          { 'icon-gray': theme === 'light' },
                                          { 'icon-blue-light': theme === 'dark' }
                                        )}
                                      />
                                    </div>
                                  </Link>
                                  <Field
                                    type={!isPassVisible ? 'password' : 'text'}
                                    className={cx(
                                      "form-control form-control-lg pl-5 rounded-lg",
                                      { 'is-invalid': ( errors.confirmPassword && touched.confirmPassword ) },
                                      { 'bg-color-firefly-2 text-color-cutty-shark-2': theme === 'dark' },
                                      { 'bg-color-mercury-2 text-color-silver-chalice': theme === 'light' }
                                    )}
                                    name="confirmPassword"
                                  />
                                  <ErrorMessage name="confirmPassword" component="div" className="invalid-feedback" />
                                </div>
                            </FormGroup>
                            <FormGroup className="position-relative mb-4 py-2">
                                <Button
                                    type="submit"
                                    color="info"
                                    className="btn btn-block btn-lg text-white"
                                    disabled={submitted}
                                >
                                    {submitted && (
                                        <span className="spinner-border spinner-border-sm"></span>
                                    )}
                                    <span>Sign Up</span>
                                </Button>
                            </FormGroup>
                        </Form>
                        <h5 
                          className={cx(
                            "text-center",
                            { 'text-color-regent-gray': theme === 'dark' },
                            { 'text-color-granny-smith': theme === 'light' },
                          )}
                          >or <br/>
                            <Link 
                              className={cx(
                                "font-weight-bold",
                                { 'text-white': theme === 'dark' },
                                { 'text-info': theme === 'light' },
                              )}
                              to={{ 
                                pathname: '/login', 
                              }}
                              >
                              Already have account?
                            </Link>
                        </h5>

                      </div>

                    </Col>
                  </Row>

                </div>
            )}
        />
    )
}


export default SignUp;

