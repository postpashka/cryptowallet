import React, { useRef, useEffect } from "react";
import SidebarLayout from '../layout/SidebarLayout';

import { useDispatch, useSelector } from "react-redux";

import { WalletCard, WalletCardSmall } from '../components/WalletCards';

import {
		Col, 
		Row,
		FormGroup,
		Label,
		Input,
		Form
} from 'reactstrap';

import cx from 'classnames';


import {ReactComponent as SearchIcon} from '../assets/icons/search.svg';

import { setAssets, getAssets, setUserAssets, setUserAssetsOrder } from "../actions/assets";

let statsItemsData = [
	{
		title: '24H Change',
		subtitle: '+$19.06'
	},
	{
		title: 'Highest Balance',
		subtitle: '$100,056.08'
	},
	{
		title: 'Portfolio Age',
		subtitle: '+2 Years'
	},
	{
		title: 'Best 24H Asset',
		subtitle: 'Bitcoin +0.35%'
	},
	{
		title: 'Worst 24H Asset',
		subtitle: 'Dash -2.46%'
	},

];


const HomeStatistic = (props) => {

	const { theme } = useSelector(state => state.Theme);

	return (
		<div className="home-statistic">
			<div className="d-flex mb-5">

				<div className="d-flex flex-column align-items-center">

					<div className="d-flex flex-row justify-content-around align-items-center mb-2">
						<div className="h3" >$2345.06</div>
						<div className="px-3 h6">USD</div>
					</div>
					<p
						className={cx(
							'h6',
							{ 'text-color-rolling-stone': theme === 'dark' },
							{ 'text-color-bitter': theme === 'light' },
						)}
					>03 Assets</p>
					<div className="position-relative w-100">				
						<div className="w-100 my-2 position-absolute d-flex  justify-content-center align-items-center">
							<Form inline>
								<FormGroup>
								  <Input 
								  	type="select" 
								  	name="select" 
								  	id="sort" 
								  	bsSize="xs"
								  	className={cx(
								  		{ 'bg-color-firefly text-color-cutty-shark-3': theme === 'dark' },
								  		{ 'bg-color-mercury-3 text-color-jelly-bean-2': theme === 'light' },
								  	)}
								  >
								    <option>Now</option>
								    <option>now</option>
								  </Input>
								</FormGroup>
							</Form>
						</div>
					</div>
					
				</div>
				<div className="d-flex flex-grow-1 align-content-stretch align-items-center m-n2 border-left border-dark">


					{

						statsItemsData.map( (item, index) => 

							<div
								key={index}
								className="flex-grow-1 px-2"
							>
								<div
									className={cx(
										"text-center px-4 py-3 rounded-lg ",
										{ 'bg-color-firefly-2': theme === 'dark' },
										{ 'bg-white': theme === 'light' },
									)}

									>
									<p
										className="text-info mb-2"
									>
										{item.title}
									</p>
									<p
										className={cx(
											'h6',
											{ 'text-color-bitter': theme === 'light' },
											{ 'text-white': theme === 'dark' },
										)}
									>{item.subtitle}</p>
								</div>
							</div>

						)

					}


					
					
				</div>

			</div>


		</div>
	);

}


const HomeAssets = (props) => {


	const dispatch = useDispatch();
	
	useEffect(() => {
	  dispatch(getAssets())
	}, [dispatch]);


	const { theme } = useSelector(state => state.Theme);

	const { assets } = useSelector(state => state.Assets);

	const { userAssets, loading, error } = useSelector(state => state.Assets);


	const draggingItem = useRef();
	const dragOverItem = useRef();

	const handleDragStart = (e, position) => {
	  draggingItem.current = position;
	};

	const handleDragEnterUserAssets = (e, position) => {
		dragOverItem.current = position;

		const updatedList = [...userAssets];

		const draggingItemContent = updatedList[draggingItem.current];
		updatedList.splice(draggingItem.current, 1);
		updatedList.splice(dragOverItem.current, 0, draggingItemContent);
		draggingItem.current = dragOverItem.current;
		dragOverItem.current = null;
		// Update PG list in database
		dispatch(setUserAssets(updatedList))
	}

	const handleDragEnterAssets = (e, position) => {
		dragOverItem.current = position;

		const updatedList = [...assets];

		const draggingItemContent = updatedList[draggingItem.current];
		updatedList.splice(draggingItem.current, 1);
		updatedList.splice(dragOverItem.current, 0, draggingItemContent);
		draggingItem.current = dragOverItem.current;
		dragOverItem.current = null;
		// Update PG list in database
		dispatch(setAssets(updatedList))

	}

	const onChange = (e) => {
		dispatch(setUserAssetsOrder(e.target.value));
	};

	return (
		<div className="home-assets flex-grow-1 h-0 mt-5">
			<Row className="h-100 overflow-hidden m-0">
				<Col sm={8} className="h-100 pl-0 pr-5">
					<div className="d-flex flex-column h-100 mh-100">
						<div className="border-bottom border-dark mb-4 pb-3">
							<h4>Your Assets</h4>
							<div className="d-flex justify-content-between align-items-center">
								<h6 
									className={cx(
										'py-1',
										{ 'text-color-oslo-gray': theme === 'dark' },
										{ 'text-color-bitter': theme === 'light' },
									)}
								>
									You currently own following assets.
								</h6>
								<Form inline> 
									<FormGroup>
									  <Label className="text-info px-2" for="sort">Sort By: </Label>
									  <Input 
									  	type="select" 
									  	name="select" 
									  	id="sort" 
									  	bsSize="xs"
									  	onChange={onChange}
									  	className={cx(
									  		{ 'bg-color-firefly text-color-cutty-shark-3': theme === 'dark' },
									  		{ 'bg-white': theme === 'light' },
									  	)}
									  >
									    <option>default</option>
									    <option>currency</option>
									    <option>price</option>
									    <option>change</option>
									    <option>balance</option>
									    <option>value</option>
									    <option>portfolio</option>
									  </Input>
									</FormGroup>
								</Form>
							</div>
						</div>
						<div className="flex-grow-1 h-0">
							<div 
								style={{overflowY: 'scroll', overflowX: 'hidden'}}
								className="h-100 mh-100 custom-scroll custom-scroll-lg"
							>

								{
									userAssets.map( (item, index) => 

										<WalletCard
											key={index}
											onDragStart={(e) => handleDragStart(e, index)}
											onDragEnter={(e) => handleDragEnterUserAssets(e, index)}
											item={item}
										/>
									)
								}
							</div>
						</div>
					</div>


				</Col>
				<Col sm={4} className="h-100 pl-0 pr-5">
					<div className="d-flex flex-column h-100 mh-100">
						<div className="border-bottom border-dark mb-4 pb-3">
							<h4>Explore Assets</h4>
							<div className="d-flex justify-content-between align-items-center">
								<h6 
									className={cx(
										'py-1',
										{ 'text-color-oslo-gray': theme === 'dark' },
										{ 'text-color-bitter': theme === 'light' },
									)}
								>
									Explore assets & add to your portfolio. 
								</h6>
							</div>
						</div>
						<div className="flex-grow-1 h-0">
							<Form className="position-relative">
								<div className="py-1 px-2 position-absolute input-icon input-icon-left">
									<SearchIcon 
										width="32px" 
										height="32px" 
										className={cx(
											'p-2',
											{ 'icon-gray': theme === 'light' },
											{ 'icon-blue-light': theme === 'dark' }
										)}
									/>
								</div>
								<Input 
									type='text'
									bsSize="sm"
									placeholder="search"
									className={cx(
										'pl-5 text-gray-600',
										{ 'bg-blue-700 text-blue-100': theme === 'dark' }
									)}
								/>
							</Form>
							<div 
								style={{overflowY: 'scroll', overflowX: 'hidden'}}
								className="h-100 mh-100 custom-scroll custom-scroll-sm"
							>

								<div>
									{
										assets.map( (item, index) => 

											<WalletCardSmall 
												key={index}
												item={item}
												onDragStart={(e) => handleDragStart(e, index)}
												onDragEnter={(e) => handleDragEnterAssets(e, index)}
												draggable
											/>
										)
									}
								</div>
							</div>
						</div>
					</div>


				</Col>
			</Row>
		</div>
	);
}



const Home = (props) => {

	return (
		<div className="page home-page">
			<SidebarLayout {...props}>
				<HomeStatistic/>
				<HomeAssets/>
			</SidebarLayout>
		</div>
	);

}

export default Home;

