import React, { Fragment } from "react";

import SidebarLayout from '../layout/SidebarLayout';


import { useSelector } from "react-redux";

import {
		Col, 
		Row,
		FormGroup,
		Label,
		Input,
		Form,
		Table
} from 'reactstrap';

import cx from 'classnames';

const TransactionsTable = (props) => {
	const { theme } = useSelector(state => state.Theme);
	return (
		<div className="home-assets flex-grow-1 h-0">
			<Row className="h-100 overflow-hidden m-0">
				<Col sm={12} className="h-100 pl-0 pr-5">
					<div className="d-flex flex-column h-100 mh-100">
						<div className="border-bottom border-dark mb-4 pb-3">
							<h4>Transaction History</h4>
							<div className="d-flex justify-content-between align-items-center">
								<h6 
									className={cx(
										'py-1',
										{ 'text-color-oslo-gray': theme === 'dark' },
										{ 'text-color-bitter': theme === 'light' },
									)}
								>
									Below is complete transaction history of your account.
								</h6>
								<Form inline> 
									<FormGroup>
									  <Label className="text-info px-2" for="sort">Type: </Label>
									  <Input 
									  	type="select" 
									  	name="select" 
									  	id="sort" 
									  	size="xs"
									  	className={cx(
									  		{ 'bg-color-firefly text-color-cutty-shark-3': theme === 'dark' },
									  		{ 'bg-white': theme === 'light' },
									  	)}
									  >
									    <option>Price</option>
									    <option>Price</option>
									  </Input>
									</FormGroup>
									<FormGroup>
									  <Label className="text-info px-2" for="sort">Assets: </Label>
									  <Input 
									  	type="select" 
									  	name="select" 
									  	id="sort" 
									  	size="xs"
									  	className={cx(
									  		{ 'bg-color-firefly text-color-cutty-shark-3': theme === 'dark' },
									  		{ 'bg-white': theme === 'light' },
									  	)}
									  >
									    <option>Price</option>
									    <option>Price</option>
									  </Input>
									</FormGroup>
								</Form>
							</div>
						</div>

						<div className="flex-grow-1 h-0">
							<div 
								style={{overflowY: 'scroll', overflowX: 'hidden'}}
								className="h-100 mh-100 custom-scroll custom-scroll-lg"
							>


								<Table 
									borderless
									className={cx(
										//'text-white'
										//{ 'table-dark': theme === 'dark' },
									)}
								>
								  <thead>
								    <tr className="text-color-oslo-gray h5">
								      <th className="p-4">Date & Time</th>
								      <th className="p-4">Transaction Type</th>
								      <th className="p-4">Crypto Amount</th>
								      <th className="p-4">USD Amount</th>
								    </tr>
								  </thead>
								  <tbody>

								  	{
								  		new Array(20)
								  			.fill(1)
								  			.map((item, index) =>

								  			<Fragment>
									  			<tr 
									  				className={cx(
									  					'mb-2 h6',
									  					{ 'bg-color-firefly-2 text-color-cutty-shark-3': theme === 'dark' },
									  					{ 'bg-color-gallery text-color-outer-space': theme === 'light' },
									  				)}
									  			>
									  			  <td className="p-4">April,09,2020,5:42:44 AM</td>
									  			  <td className="p-4">Sent BTC</td>
									  			  <td className="p-4">0.024 BTC</td>
									  			  <td className="p-4">$50.00 USD</td>
									  			</tr>
									  			<tr>
									  				<td className="p-2"></td>
									  			</tr>
								  			</Fragment>
								  		)
								  	}

								  </tbody>
								</Table>


							</div>
						</div>

					</div>
				</Col>
			</Row>
		</div>
	);
}

const Transactions = (props) => {

	return (
		<div className="page login-page">
			<SidebarLayout {...props}>
				<TransactionsTable/>
			</SidebarLayout>
		</div>
	);
}

export default Transactions;

