import React, { useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";

import { Link } from "react-router-dom";

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";

import { required, emailVal } from '../components/FormComponents';
import {
		Button,
		Col, 
		Row,
		FormGroup,
		Label,
} from 'reactstrap';

import cx from 'classnames';

import login from '../assets/imgs/login-bg.jpg';

import emailIcon from '../assets/icons/email.svg';
import passwordIcon from '../assets/icons/password.svg';

import {ReactComponent as EyeIcon} from '../assets/icons/eye.svg';

import { authActions } from "../actions/auth";


const Login = (props) => {

	const { theme } = useSelector(state => state.Theme);

	const dispatch = useDispatch();

	const form = useRef();
	const checkBtn = useRef();

	const [email, setEmail] = useState("user@test.com");
	const [password, setPassword] = useState("testtest");
	const [isPassVisible, setPassVisible] = useState(false);
	const [loading, setLoading] = useState(false);

	const { message } = useSelector(state => state.Message);

	const onChangeEmail = (e) => {
		const email = e.target.value;
		setEmail(email);
	};

	const onChangePassword = (e) => {
		const password = e.target.value;
		setPassword(password);
	};

	const handleLogin = (e) => {
		e.preventDefault();

		setLoading(true);

		form.current.validateAll();

		if (checkBtn.current.context._errors.length === 0) {
			setLoading(false);
			dispatch(authActions.login())
			props.history.push('/home')
		} else {
		setLoading(false);
		}
	};

	return (
		<div className="page login-page">

			<Row className="m-0 h-100">
				<Col sm={6} className="p-0">
					<div 
						className="card-img-overlay"
						style={{
								 background: `url(${login})`,
								 backgroundSize: 'cover'
							 }}
					/>
					<div 
						className="card-img-overlay bg-color-black-pearl opacity-5"
					/>
				</Col>
				<Col sm={6} 
					className={cx(
						"d-flex align-items-center justify-content-center p-0",
						{ 'bg-color-alabaster': theme === 'light' },
					)}
				>
					<div className="d-flex w-50 flex-column"> 
						<h2 className="text-center mb-3 font-weight-bold">Log in</h2>
						<h5 
							className={cx(
								"text-center",
								{ 'text-color-river-bed': theme === 'dark' },
								{ 'text-color-lemon-grass': theme === 'light' },
							)}
						>
							Enter your Email and Password
						</h5>
						<Form onSubmit={handleLogin} ref={form} className="w-100 my-5">
							<FormGroup className="mb-4">
								<Label
									className={cx(
										"small p-2",
										{ 'text-color-cutty-shark': theme === 'dark' },
										{ 'text-color-lemon-grass': theme === 'light' }
									)}
									for="email"
								>
									Email
								</Label>
								<div className="position-relative">
									<img src={emailIcon} className="input-icon input-icon-left position-absolute p-3" alt="placeholder"/>
									<Input
										type="email"
										className={cx(
											"form-control form-control-lg pl-5 rounded-lg",
											{ 'bg-color-firefly-2 text-color-cutty-shark-2': theme === 'dark' },
											{ 'bg-color-mercury-2 text-color-silver-chalice': theme === 'light' }
										)}
										name="email"
										value={email}
										onChange={onChangeEmail}
										validations={[required, emailVal]}
									/>
								</div>
							</FormGroup>
							<FormGroup className="position-relative mb-4">
								<Label 
									className={cx(
										"small p-2",
										{ 'text-color-cutty-shark': theme === 'dark' },
										{ 'text-color-lemon-grass': theme === 'light' },

									)}
									for="password"
								>
									Password
								</Label>
								<div className="position-relative">
									<img src={passwordIcon} className="input-icon input-icon-left position-absolute p-3" alt="placeholder"/>
									<Link
										to="#"
										onClick={()=>setPassVisible(!isPassVisible)}
									>

										<div className="input-icon input-icon-right position-absolute p-3">
											<EyeIcon 
												className={cx(
													{ 'icon-gray': theme === 'light' },
													{ 'icon-blue-light': theme === 'dark' }
												)}
											/>
										</div>
									</Link>
									<Input
										type={!isPassVisible ? 'password' : 'text'}
										className={cx(
											"form-control form-control-lg pl-5 rounded-lg",
											{ 'bg-color-firefly-2 text-color-cutty-shark-2': theme === 'dark' },
											{ 'bg-color-mercury-2 text-color-silver-chalice': theme === 'light' }

										)}
										name="password"
										value={password}
										onChange={onChangePassword}
										validations={[required]}
									/>
								</div>
								<p className="text-right p-2">
									<Link 
										className={cx(
											"small",
											{ 'text-white': theme === 'dark' },
											{ 'text-gray-600': theme === 'light' },

										)}
										to={{ 
										pathname: '/forgotPassword'
										}}
										>
										Forgot password?
									</Link>
								</p>
							</FormGroup>
							<FormGroup className="position-relative mb-4 py-2">
								<Button
									color="info"
									className="btn btn-block btn-lg text-white"
									disabled={loading}
								>
									{loading && (
										<span className="spinner-border spinner-border-sm"></span>
									)}
									<span>Login</span>
								</Button>
							</FormGroup>
							{message && (
								<div className="form-group">
									<div className="alert alert-danger" role="alert">
										{message}
									</div>
								</div>
							)}
							<CheckButton style={{ display: "none" }} ref={checkBtn} />
						</Form>
						<h5 
							className={cx(
								"text-center",
								{ 'text-color-regent-gray': theme === 'dark' },
								{ 'text-color-granny-smith': theme === 'light' },
							)}
							>
							<span>Don’t have an account? </span>
							<Link 
								className={cx(
									"font-weight-bold",
									{ 'text-white': theme === 'dark' },
									{ 'text-info': theme === 'light' },
								)}
								to={{ 
									pathname: '/signup', 
							}}
							>Sign up now!</Link>
						</h5>
					</div>

				</Col>
			</Row>
		</div>
	);
}

export default Login;

