import React, { useState, useRef } from "react";
import { useSelector } from "react-redux";

import { Link } from "react-router-dom";

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";

import { required, emailVal } from '../components/FormComponents';
import {
    Button,
    Col, 
    Row,
    FormGroup,
    Label,
} from 'reactstrap';

import cx from 'classnames';

import signUp from '../assets/imgs/signup-bg.jpg';

import emailIcon from '../assets/icons/email.svg';



const ForgotPass = (props) => {

  const { theme } = useSelector(state => state.Theme);

  const form = useRef();
  const checkBtn = useRef();

  const [email, setEmail] = useState("user@test.com");
  const [loading, setLoading] = useState(false);

  const { message } = useSelector(state => state.Message);

  const onChangeEmail = (e) => {
    const email = e.target.value;
    setEmail(email);
  };


  const handleLogin = (e) => {
    e.preventDefault();

    setLoading(true);

    form.current.validateAll();

    if (checkBtn.current.context._errors.length === 0) {
          setLoading(false);
    } else {
      setLoading(false);
    }
  };

  return (
    <div className="page restore-password-page">
      <Row className="m-0 h-100">
        <Col sm={6} className="p-0">
          <div 
            className="card-img-overlay"
            style={{
                   background: `url(${signUp})`,
                   backgroundSize: 'cover'
                 }}
          />
          <div 
            className="card-img-overlay bg-color-black-pearl opacity-5"
          />
        </Col>
        <Col sm={6} 
          className={cx(
            "d-flex align-items-center justify-content-center p-0",
            { 'bg-color-alabaster': theme === 'light' },
          )}
        >
          <div className="d-flex flex-column w-50"> 
            <h3 className="text-center mb-3 font-weight-bold">Restore pass</h3>
            <h6
              className={cx(
                "text-center",
                { 'text-color-river-bed': theme === 'dark' },
                { 'text-color-lemon-grass': theme === 'light' },

              )}
            >
              Enter your Email
            </h6>
            <Form onSubmit={handleLogin} ref={form} className="w-100 my-5">
                <FormGroup className="mb-4">
                    <Label
                      className={cx(
                        "small p-2",
                        { 'text-color-cutty-shark': theme === 'dark' },
                        { 'text-color-lemon-grass': theme === 'light' },
                      )} 
                      for="email"
                    >
                      Email
                    </Label>
                    <div className="position-relative">
                    <img src={emailIcon} className="input-icon input-icon-left position-absolute p-3" alt="placeholder"/>
                      <Input
                          type="email"
                          className={cx(
                            "form-control form-control-lg pl-5 rounded-lg",
                            { 'bg-color-firefly-2 text-color-cutty-shark-2': theme === 'dark' },
                            { 'bg-color-mercury-2 text-color-silver-chalice': theme === 'light' }
                          )}
                          name="email"
                          value={email}
                          onChange={onChangeEmail}
                          validations={[required, emailVal]}
                      />
                    </div>
                </FormGroup>
                <FormGroup className="position-relative mb-4 py-2">
                    <Button
                        color="info"
                        className="btn btn-block btn-lg text-white"
                        disabled={loading}
                    >
                        {loading && (
                            <span className="spinner-border spinner-border-sm"></span>
                        )}
                        <span>Send</span>
                    </Button>
                </FormGroup>
                {message && (
                    <div className="form-group">
                        <div className="alert alert-danger" role="alert">
                          {message}
                        </div>
                    </div>
                )}
                <CheckButton style={{ display: "none" }} ref={checkBtn} />


            </Form>
            <h5 
              className={cx(
                "text-center",
                { 'text-color-regent-gray': theme === 'dark' },
                { 'text-color-granny-smith': theme === 'light' },
              )}
            >
              <span>Don’t have an account? </span>
              <Link 
                className={cx(
                  "font-weight-bold",
                  { 'text-white': theme === 'dark' },
                  { 'text-info': theme === 'light' },
                )}
                to={{ 
                  pathname: '/signup', 
              }}
              >Sign up now!</Link>
            </h5>
          </div>

        </Col>
      </Row>
    </div>
  );
}

export default ForgotPass;

