import React, { Fragment, useState, useRef } from "react";
import {  Route } from 'react-router-dom';
import SidebarLayout from '../layout/SidebarLayout';

import { Link } from "react-router-dom";

import { WalletCard } from '../components/WalletCards';
import WalletChart from '../components/WalletChart';

import { useDispatch, useSelector } from "react-redux";

import {
		Col, 
		Row,
		FormGroup,
		Label,
		Input,
		Form,
		Media
} from 'reactstrap';


import { 
  SendModal,
  ReceiveModal,
  SuccessModal,
  ErrorModal,
  LoadingModal 
} from '../components/Modals';

import cx from 'classnames';

import Tabs, { TabPane } from 'rc-tabs';

import {ReactComponent as SearchIcon} from '../assets/icons/search.svg';

import {ReactComponent as ArrowUp} from '../assets/icons/arrow-up.svg';
import {ReactComponent as ArrowDown} from '../assets/icons/arrow-down.svg';


import currencyList from '../components/CurrencyList';


import { setUserAssets } from "../actions/assets";

import { setUserAssetsOrder } from "../actions/assets";


let transactionsItems = [
	{
		operation: 'Sent BTC',
		date:  'April,09,2020,5:42:44 AM',
		sum1: '0.024 BTC',
		sum2: '$50.00 USD',
	},
	{
		operation: 'Received BTC',
		date:  'April,09,2020,5:42:44 AM',
		sum1: '0.024 BTC',
		sum2: '$50.00 USD',
	},
]

transactionsItems = transactionsItems.concat(transactionsItems);
transactionsItems = transactionsItems.concat(transactionsItems);
transactionsItems = transactionsItems.concat(transactionsItems);



const WalletAssets = (props) => {

	const { theme } = useSelector(state => state.Theme);

	const dispatch = useDispatch();


	const draggingItem = useRef();
	const dragOverItem = useRef();

	const { userAssets } = useSelector(state => state.Assets);

	const [activeWalletItems, setActiveWalletItems] = useState( new Array( userAssets.length ).fill(false) );

	const [activeWalletItemIndex, setActiveWalletItemIndex] = useState( -1 );

	const handleDragStart = (e, position) => {
	  draggingItem.current = position;
	};

	const handleDragEnterUserAssets = (e, position) => {

	  dragOverItem.current = position;

	  const updatedList = [...userAssets];
	  const updatedActiveList = [...activeWalletItems];

	  const draggingItemContent = updatedList[draggingItem.current];
	  const draggingItemContent2 = updatedActiveList[draggingItem.current];

	  updatedList.splice(draggingItem.current, 1);
	  updatedList.splice(dragOverItem.current, 0, draggingItemContent);

	  updatedActiveList.splice(draggingItem.current, 1);
	  updatedActiveList.splice(dragOverItem.current, 0, draggingItemContent2);



	  draggingItem.current = dragOverItem.current;
	  dragOverItem.current = null;
	  // Update PG list in database

	  dispatch(setUserAssets(updatedList))

	  setActiveWalletItems(updatedActiveList);
	  setActiveWalletItemIndex(  activeWalletItems.indexOf(true) );

	};



	function cardClick(index) {
		if ( activeWalletItems[index] ) {
			const updatedList = [...activeWalletItems];
			updatedList[index] = false;
		    setActiveWalletItems(updatedList)
		    setActiveWalletItemIndex(  updatedList.indexOf(true) );
		} else {

			const updatedList = [...activeWalletItems];
			updatedList.fill(false)
			updatedList[index] = true;
		    setActiveWalletItems(updatedList)
		    setActiveWalletItemIndex(  updatedList.indexOf(true) );

		}

	}

	const onChange = (e) => {
		dispatch(setUserAssetsOrder(e.target.value));
	};



	return (
		<div className="flex-grow-1 h-0">
			<Row className="h-100 m-0">
				<Col sm={8} className="h-100 pl-0 pr-5">
					<div className="d-flex flex-column h-100 mh-100">
						<div className="border-bottom border-dark mb-4 pb-3">
							<h4>Your Wallet</h4>
							<div className="d-flex justify-content-between align-items-center">
								<h6 
									className={cx(
										'py-1',
										{ 'text-color-oslo-gray': theme === 'dark' },
										{ 'text-color-bitter': theme === 'light' },
									)}
								>
									Select below assets to send, receive or exchange.
								</h6>
								<Form inline> 
									<FormGroup>
									  <Label className="text-info px-2" for="sort">Sort By: </Label>
									  <Input 
									  	type="select" 
									  	name="select" 
									  	id="sort" 
									  	bsSize="xs"
									  	onChange={onChange}
									  	className={cx(
									  		{ 'bg-color-firefly text-color-cutty-shark-3': theme === 'dark' },
									  		{ 'bg-white': theme === 'light' },
									  	)}
									  >
									  	<option>default</option>
									  	<option>currency</option>
									  	<option>price</option>
									  	<option>change</option>
									  	<option>balance</option>
									  	<option>value</option>
									  	<option>portfolio</option>
									  </Input>
									</FormGroup>
								</Form>
							</div>
						</div>

						<Form className="position-relative py-1 mb-4">
							<div className="py-2 px-2 position-absolute input-icon input-icon-left">
								<SearchIcon 
									width="32px" 
									height="32px" 
									className={cx(
										'p-2',
										{ 'icon-gray': theme === 'light' },
										{ 'icon-blue-light': theme === 'dark' }
									)}
								/>
							</div>
							<Input 
								type='text'
								bsSize="sm"
								placeholder="search"
								className={cx(
									'pl-5 text-gray-600',
									{ 'bg-blue-700 text-blue-100': theme === 'dark' }
								)}
							/>
						</Form>

						<div className="flex-grow-1 h-0">
							<div 
								style={{overflowY: 'scroll', overflowX: 'hidden'}}
								className="h-100 mh-100 custom-scroll custom-scroll-lg"
							>
								


								{
									userAssets.map( (item, index) => 

										<div

											className={cx(
												'wallet-card-container',
												{ 'active': activeWalletItems.indexOf(true) === index  }
											)}
											onClick={()=>cardClick(index)}
											onDragStart={(e) => handleDragStart(e, index)}
											onDragEnter={(e) => handleDragEnterUserAssets(e, index)}


										>


											<WalletCard item={item}/>
										</div>
									)
								}
							</div>
						</div>
					</div>


				</Col>

				{
					( activeWalletItemIndex !== -1 ) && 

					(
						<Col sm={4} className="h-100 pl-0 pr-5">
							<div className="d-flex flex-column h-100 mh-100">
								<div className="border border-dark mb-4">
									<Media className="p-3">

									    <Media left top href="#">
									      <Media 
									      	width="50"
									      	height="50"
									      	object 
									      	src={currencyList[userAssets[activeWalletItemIndex].currency].img} 
									      	alt="Generic placeholder image" 
									      />
									    </Media>
									    <Media body className="px-3">
									      <Media className="small">
									        { currencyList[userAssets[activeWalletItemIndex].currency].name } 
									      </Media>
									      <Media className="h6">
									        0.0819132 {userAssets[activeWalletItemIndex].currency}
									      </Media>
									      $651.49
									    </Media>
									  </Media>
								</div>
								<div className="border-bottom border-dark mb-4 pb-3">
									<div className="d-flex">
										<div className="flex-grow-1 px-1">
											<Link 
												role="button"
												className="btn btn-block btn-primary"
											  	to={{ 
												    search: "?send=true" 
											  	}}
											  >
											  <ArrowUp className="mr-2"/>
											  Send
											</Link>
										</div>
										<div className="flex-grow-1 px-1">
											<Link 
												role="button"
												className="btn btn-block btn-success"
											  	to={{ 
												    search: "?receive=true" 
											  	}}
											  >
											  <ArrowDown className="mr-2"/>
											  Recive
											</Link>
										</div>
									</div>
								</div>
								<div className="flex-grow-1 h-0">
									<Tabs 
										className="sidebar-tabs"
										defaultActiveKey="1" 
									>
									  <TabPane tab="Information" key="1">
									  	<WalletChart/>
									  	<div className="small my-5">
									  		<span className="py-1"> Description:</span>
										    <p
										    	className={cx(
										    		{ 'text-color-oslo-gray': theme === 'dark' },
										    		{ 'text-color-bitter': theme === 'light' },
										    	)}
										    > 
										    	Bitcoin uses peer-to-peer technology to operate with no central authority or banks; managing transactions and the issuing of bitcoins is carried out collectively by the network. Bitcoin is open-source; its design is public, nobody owns or controls Bitcoin and everyone can take part. Through many of its unique properties, Bitcoin allows exciting uses that could not be covered by any previous payment system. 
										    </p> 
									  	</div>
									  </TabPane>
									  <TabPane tab="Transactions" key="2">
									  	<Form inline>
									  		<FormGroup className="ml-auto">
									  		  <Label className="text-info px-2" for="sort">Type: </Label>
									  		  <Input 
									  		  	type="select" 
									  		  	name="select" 
									  		  	id="sort" 
									  		  	bsSize="xs"
									  		  	className={cx(
									  		  		{ 'bg-blue-700 text-blue-100': theme === 'dark' }
									  		  	)}
									  		  >
									  		    <option>All</option>
									  		    <option>Price</option>
									  		  </Input>
									  		</FormGroup>
									  	</Form>
									  	<div 
									  		style={{overflowY: 'scroll', overflowX: 'hidden'}}
									  		className="h-100 mh-100 custom-scroll custom-scroll-sm"
									  	>
										  	{
										  		transactionsItems.map( (item) => 

										  			<div 
										  				className={cx(
										  					'py-3 px-4 my-2 rounded-lg',
										  					{ 'bg-color-firefly-2 text-white': theme === 'dark' },
										  					{ 'bg-white text-color-tundora': theme === 'light' }
										  				)}
										  			>
										  				<p className="d-flex justify-content-between">
										  					<h6>{item.operation}</h6>
										  					<span className="small">{item.sum1}</span>
										  				</p>
										  				<p className="d-flex justify-content-between">
										  					<span 
										  						className={cx(
										  							'small',
										  							{ 'text-color-delta': theme === 'light' }
										  						)}
										  					>
										  						{item.date}
										  					</span>
										  					<span className="small">{item.sum2}</span>
										  				</p>
										  			</div>

										  		)
										  	}	
									  	</div>


									  </TabPane>

									</Tabs>
								</div>
							</div>
						</Col>

					)
				}

			</Row>

			{
				( activeWalletItems.indexOf(true) !== -1 ) && 
				(
					<Fragment>
						<Route path="/" component={ 
							(props) => 
								<SendModal 
									currency={ currencyList[userAssets[activeWalletItemIndex].currency] } { ...props} 
								/> 
						}/>
						<Route path="/" component={ 
							(props) => 
								<ReceiveModal
									currency={ currencyList[userAssets[activeWalletItemIndex].currency] } { ...props} 
								/> 
						}/>
						<Route path="/" component={SuccessModal}/>
						<Route path="/" component={ErrorModal}/>
						<Route path="/" component={LoadingModal}/>
					</Fragment>
				)
			}

		</div>
	);
}



const Wallet = (props) => {

	return (
		<div className="page login-page">
			<SidebarLayout {...props}>
				<WalletAssets/>
			</SidebarLayout>
		</div>
	);
}


export default Wallet;

