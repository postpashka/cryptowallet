import React from "react";

import { useSelector } from "react-redux";

import cx from 'classnames';

import {  Link } from 'react-router-dom';

import {
    Row,
    Button,
    Col,
    Input
} from 'reactstrap';



import passwordIcon from '../../assets/icons/password.svg';


const SettingsRestore = (props) => {


	const { theme } = useSelector(state => state.Theme);

	return (
		<div className="backup-slide backup-slide-one">

			<div>
				<h5>Your Recovery Phrase</h5>
				<div className="d-flex justify-content-between align-items-center">
					<h6 
						className={cx(
							'py-1',
							{ 'text-color-oslo-gray': theme === 'dark' },
							{ 'text-color-bitter': theme === 'light' },
						)}
					>
						Press "Eye" icon to view the recovery phrase word.
					</h6>
				</div>				
			</div>

			<Row className="my-5">


				{
					
					[1, 2].fill(1)
						.map((colItem, colIndex) => 


							<Col sm={6}>


								{
									new Array(6)
										.fill(1)
										.map((item, index) =>
											<div className="position-relative mb-2">
												<span className="input-icon input-icon-left position-absolute p-3">
													{ (index+1)+colIndex*6 }
												</span>
												<Link
													href="#"
													//onClick={ ()=>setPassVisibilityItem( index + colIndex*6 ) }
												>

													<div className="input-icon input-icon-right position-absolute p-2 m-1">
														<img src={passwordIcon} alt="placeholder"/>
													</div>
												</Link>
												<Input
													//type={!passVisibilityArray[index + colIndex*6] ? 'password' : 'text'}
													className={cx(
														"form-control pl-5 rounded-lg",
														{ 'bg-color-firefly-2 text-color-cutty-shark-2': theme === 'dark' },
														{ 'bg-color-mercury-2 text-color-silver-chalice': theme === 'light' }

													)}
													name="password"
													//value={ passArray[index + colIndex*6] }
												/>
											</div>
										)
								}



							</Col>


						)

				}



			</Row>

			<div className="my-5">
				<Button
					color="primary"
					className="px-5 btn-lg"
				>Restore</Button>
				<span
					className="px-5 text-primary"
				>Your account has been restored.</span>
			</div>

		</div>
	);
}

export default SettingsRestore;

