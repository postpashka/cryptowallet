import React from "react";

import { useSelector } from "react-redux";

import cx from 'classnames';


import SidebarLayout from '../../layout/SidebarLayout';



import SettingsLanguage from './SettingsLanguage';
import SettingsCurrency from './SettingsCurrency';

import SettingsBackup from './SettingsBackup';
import SettingsRestore from './SettingsRestore';




import Tabs, { TabPane } from 'rc-tabs';

const SettingPageTitle = () => {

	const { theme } = useSelector(state => state.Theme);

	return (
		<div>
			<h4>Settings</h4>
			<div className="d-flex justify-content-between align-items-center">
				<h6 
					className={cx(
						'py-1',
						{ 'text-color-oslo-gray': theme === 'dark' },
						{ 'text-color-bitter': theme === 'light' },
					)}
				>
					Manage your account.
				</h6>
			</div>
		</div>

	)
}




const Settings = (props) => {


	var callback = function(key) {};

	return (
		<div className="page setting-page">
			<SidebarLayout {...props}>
				<Tabs 
					className="page-tabs"
					tabBarExtraContent={<SettingPageTitle/>}
					defaultActiveKey="1" onChange={callback}
				>
					<TabPane tab="Currency" key="1">
				  		<div className="py-5">
					  		<div className="py-5">
						    	<SettingsCurrency/>
					  		</div>
				  		</div>
					</TabPane>
					<TabPane tab="Language" key="2">
				  		<div className="py-5">
					  		<div className="py-5">
						    	<SettingsLanguage/>
					  		</div>
				  		</div>
					</TabPane>
					<TabPane tab="Backup" key="3">
				  		<div className="py-5">
					  		<div className="py-5">
				    			<SettingsBackup/>
					  		</div>
				  		</div>
					</TabPane>
					<TabPane tab="Restore" key="4">
				  		<div className="py-5">
					  		<div className="py-5">
				    			<SettingsRestore/>
					  		</div>
				  		</div>
					</TabPane>

				 </Tabs>
			</SidebarLayout>
		</div>
	);
}

export default Settings;

