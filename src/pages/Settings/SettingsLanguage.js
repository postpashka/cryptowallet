import React from "react";

import { useSelector } from "react-redux";

import cx from 'classnames';

import {  Link } from 'react-router-dom';

import {
    Row,
    Label,
    Button,
    FormGroup,
    Col,
    Form,
} from 'reactstrap';


import CustomSelect from '../../components/CustomSelect';


const SettingsLanguage = (props) => {

	const { theme } = useSelector(state => state.Theme);

	return (
		<Row>
			<Col sm={5}>
				<div className="page setting-page settings-currency">
					<div>
						<h5>Default Language</h5>
						<div className="d-flex justify-content-between align-items-center">
							<h6 
								className={cx(
									'py-1',
									{ 'text-color-oslo-gray': theme === 'dark' },
									{ 'text-color-bitter': theme === 'light' },
								)}
							>
								Select your default Language below.
							</h6>
						</div>
					</div>

					<Form>
						<FormGroup className="my-4">
						    <Label
						        className={cx(
						            "small py-1",
						            { 'text-color-white': theme === 'dark' },
						            { 'text-color-delta': theme === 'light' }
						        )}
						        for="language"
						    >
						         Language
						    </Label>
						    <div className="position-relative">
						    	<CustomSelect 
						    		options={

							    		[
							    		  { value: 'English (United States)', label: 'English (United States)' },
							    		  { value: 'English (USA)', label: 'English (USA)' },
							    		]
							    	}

						    	/>
						    </div>
						</FormGroup>
						<FormGroup>
							<h6 
								className={cx(
									'py-1',
									{ 'text-color-oslo-gray': theme === 'dark' },
									{ 'text-color-bitter': theme === 'light' },
								)}
							>
								Your Default Language is:
								<Button
									color="link"
									className="px-2"
								>English (United States)</Button>
							</h6>
						</FormGroup>

						<FormGroup className="my-5">
						    <Link 
						        size="lg"
						        role="button"
						        className="btn btn-primary w-50 btn-lg"
						      >
						      Save
						    </Link>
						</FormGroup>

					</Form>

				</div>
			</Col>
		</Row>
	);
}



export default SettingsLanguage;

