import React, { useState } from "react";

import { useSelector } from "react-redux";

import cx from 'classnames';

import {  Link } from 'react-router-dom';

import {
    Row,
    Label,
    Button,
    FormGroup,
    Col,
    Input
} from 'reactstrap';


import {ReactComponent as EyeIcon} from '../../assets/icons/eye.svg';


const SettingsBackup = (props) => {

	const { theme } = useSelector(state => state.Theme);

	const [activeSlide, setActiveSlide] = useState(1);

	const [isCheckboxActive, setIsCheckboxActive] = useState(false);

	const [passVisibilityArray, setPassVisibilityArray] = useState(  new Array(12).fill(false) );
	const [passArray] = useState(  new Array(12).fill('secret') );


	function setPassVisibilityItem(index) {

		let tmpArr = [...passVisibilityArray]

		tmpArr[index] = !tmpArr[index]

		setPassVisibilityArray(tmpArr)

	}

	function hideAllPass(index) {

/*		let tmpArr = [...passVisibilityArray]

		passVisibilityArray[index] = !passVisibilityArray[index]*/

		setPassVisibilityArray( new Array(12).fill(false) )

	}


	return (


		<Row>
			<Col sm={10}>

				<div className="backup-slides">


					{
						(activeSlide === 1 ) &&
							<div className="backup-slide backup-slide-one">

								<div>
									<h5>Backup</h5>
									<div className="d-flex justify-content-between align-items-center">
										<h6 
											className={cx(
												'py-1',
												{ 'text-color-oslo-gray': theme === 'dark' },
												{ 'text-color-bitter': theme === 'light' },
											)}
										>
											The 12 words (your “seed” or recovery phrase) are your master seed that generates all your wallet’s addresses and private keys. In other words, with this phrase, ANYONE can access your wallet’s funds anytime and anywhere from DigitalCurrency.CRYPTO or another wallet.   
										</h6>
									</div>				
								</div>

								<div className="my-5">
									<h6 
										className={cx(
											'py-1',
											{ 'text-color-oslo-gray': theme === 'dark' },
											{ 'text-color-bitter': theme === 'light' },
										)}
									>
										<span
											className="px-2 text-primary"
										>Recovery Phrase:</span>
										<Button
											onClick={()=>setActiveSlide(2)}
											color="primary"
											className="px-5 btn-lg"
										>View</Button>
									</h6>
								</div>

							</div>

					}


					{
						(activeSlide === 2 ) &&

						<div className="backup-slide backup-slide-two">
							<div>
								<h5>View Recovery Phrase</h5>
								<div className="d-flex justify-content-between align-items-center">
									<h6 
										className={cx(
											'py-1',
											{ 'text-color-oslo-gray': theme === 'dark' },
											{ 'text-color-bitter': theme === 'light' },
										)}
									>
										Are you sure you want to view your 12 word recovery phrase?If anyone sees or gets ahold of it, they can steal your funds. Its is for your eyes only!
									</h6>
								</div>				
							</div>
							<FormGroup check inline>
							  <Label 
							  	check
							  	className={cx(
							  		'py-4',
							  		{ 'text-color-oslo-gray': theme === 'dark' },
							  		{ 'text-color-bitter': theme === 'light' },
							  	)}
							  >
							     <Input 
							     	type="checkbox" 
							     	checked={isCheckboxActive}
							     	onClick={()=>setIsCheckboxActive(!isCheckboxActive)}
							     /> 
							     I have read and understand the risks of showing my recovery phrase
							  </Label>
							</FormGroup>
							<div className="my-3">
								<h6 
									className={cx(
										'py-1',
										{ 'text-color-oslo-gray': theme === 'dark' },
										{ 'text-color-bitter': theme === 'light' },
									)}
								>
									<div className="mx-n3">
										<Button
											onClick={()=>setActiveSlide(3)}
											color="primary"
											className="px-5 mx-3 btn-lg"
											disabled={!isCheckboxActive}
										>View</Button>
										<Button
											onClick={()=>setActiveSlide(1)}
											outline
											color="primary"
											className="px-5 mx-3 btn-lg"
										>Cancel</Button>
									</div>
								</h6>
							</div>

						</div>

					}


					{	(activeSlide === 3 ) &&
							<div className="backup-slide backup-slide-one">

								<div>
									<h5>Your Recovery Phrase</h5>
									<div className="d-flex justify-content-between align-items-center">
										<h6 
											className={cx(
												'py-1',
												{ 'text-color-oslo-gray': theme === 'dark' },
												{ 'text-color-bitter': theme === 'light' },
											)}
										>
											Press "Eye" icon to view the recovery phrase word.
										</h6>
									</div>				
								</div>

								<Row className="my-5">


									{
										
										[1, 2].fill(1)
											.map((colItem, colIndex) => 


												<Col sm={6}>


													{
														new Array(6)
															.fill(1)
															.map((item, index) =>
																<div className="position-relative mb-2">
																	<span className="input-icon input-icon-left position-absolute p-3">
																		{ (index+1)+colIndex*6 }
																	</span>
																	<Link
																		href="#"
																		onClick={ ()=>setPassVisibilityItem( index + colIndex*6 ) }
																	>

																		<div className="input-icon input-icon-right position-absolute p-2 m-1">
																			<EyeIcon 
																				className={cx(
																					{ 'icon-gray': theme === 'light' },
																					{ 'icon-blue-light': theme === 'dark' }
																				)}
																			/>
																		</div>
																	</Link>
																	<Input
																		type={!passVisibilityArray[index + colIndex*6] ? 'password' : 'text'}
																		className={cx(
																			"form-control pl-5 rounded-lg",
																			{ 'bg-color-firefly-2 text-color-cutty-shark-2': theme === 'dark' },
																			{ 'bg-color-mercury-2 text-color-silver-chalice': theme === 'light' }

																		)}
																		name="password"
																		value={ passArray[index + colIndex*6] }
																		//onChange={onChangePassword}
																		//validations={[required]}
																	/>
																</div>
															)
													}



												</Col>


											)

									}



								</Row>

								<div className="my-5">
									<Button
										onClick={hideAllPass}
										color="primary"
										className="px-5 btn-lg"
									>Hide All</Button>
								</div>

							</div>


					}


				</div>

			</Col>
		</Row>

	);
}




export default SettingsBackup;

