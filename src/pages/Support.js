import React from "react";

import { useSelector } from "react-redux";

import cx from 'classnames';

import {  Link } from 'react-router-dom';

import {
    Row,
    Label,
    FormGroup,
    Col,
    Form,
    Input
} from 'reactstrap';


import SidebarLayout from '../layout/SidebarLayout';

import CustomSelect from '../components/CustomSelect';


const Support = (props) => {

	const { theme } = useSelector(state => state.Theme);

	return (

		<div className="page login-page">
			<SidebarLayout {...props}>
				<div className="home-assets flex-grow-1 h-0">
					<Row className="h-100 m-0">
						<Col sm={12} className="h-100 pl-0 pr-5">
							<div className="d-flex flex-column h-100 mh-100">
								<div className="border-bottom border-dark pb-3 mb-n3">
									<h4>Support</h4>
									<div className="d-flex justify-content-between align-items-center">
										<h6 
											className={cx(
												'py-1',
												{ 'text-color-oslo-gray': theme === 'dark' },
												{ 'text-color-bitter': theme === 'light' },
											)}
										>
											We are available for your help 24/7. Use below options to get our support.
										</h6>
									</div>
								</div>

						  		<div className="py-5">
							  		<div className="py-5">

										<div>
											<h4>Email Us</h4>
											<div className="d-flex justify-content-between align-items-center">
												<h6 
													className={cx(
														'py-1',
														{ 'text-color-oslo-gray': theme === 'dark' },
														{ 'text-color-bitter': theme === 'light' },
													)}
												>
													<span>Simply email us at </span>
													<Link >digitalcurrency@uplymedia.com</Link>
												</h6>
											</div>
											<h4 className="my-3">Or fill out this form, we'll quickly get back to you</h4>
										</div>

										<Form>
											<Row className="my-4">

												<Col sm={6}>

													<FormGroup className="mb-4">
													    <Label
													        className={cx(
													            "small py-1",
													            { 'text-color-white': theme === 'dark' },
													            { 'text-color-delta': theme === 'light' }
													        )}
													        for="sendTo"
													    >
													         Subject
													    </Label>
													    <div className="position-relative">
													    	<CustomSelect 
													    		options={

														    		[
														    		  { value: 'Subject 1', label: 'Subject 1' },
														    		  { value: 'Subject 2', label: 'Subject 2' },
														    		]
														    	}

													    	/>
													    </div>
													</FormGroup>
													<FormGroup className="mb-4">
														<Label
															className={cx(
															    "small py-1",
															    { 'text-color-white': theme === 'dark' },
															    { 'text-color-delta': theme === 'light' }
															)}
															for="email"
														>
															Full Name
														</Label>
														<div className="position-relative">
															<Input
																type="email"
																className={cx(
																	"form-control form-control-lg rounded-lg",
																	{ 'bg-color-firefly-2 text-color-cutty-shark-2': theme === 'dark' },
																	{ 'bg-color-mercury-2 text-color-bitter': theme === 'light' }
																)}
																name="email"
															/>
														</div>
													</FormGroup>
													<FormGroup className="mb-0">
														<Label
															className={cx(
																"small py-1",
																{ 'text-color-white': theme === 'dark' },
																{ 'text-color-delta': theme === 'light' }
															)}
															for="email"
														>
															Email
														</Label>
														<div className="position-relative">
															<Input
																type="email"
																className={cx(
																	"form-control form-control-lg rounded-lg",
																	{ 'bg-color-firefly-2 text-color-cutty-shark-2': theme === 'dark' },
																	{ 'bg-color-mercury-2 text-color-bitter': theme === 'light' }
																)}
																name="email"
																//value={email}
																//onChange={onChangeEmail}
																//validations={[required, emailVal]}
															/>
														</div>
													</FormGroup>
												</Col> 
												<Col sm={6} className="d-flex flex-column">
													<Label
														className={cx(
															"small py-1",
															{ 'text-color-white': theme === 'dark' },
															{ 'text-color-delta': theme === 'light' }
														)}
														for="email"
													>
														Message
													</Label>
													<div className="flex-grow-1 h-0">
														<Input 
															className={cx(
																"form-control form-control-lg rounded-lg h-100",
																{ 'bg-color-firefly-2 text-color-cutty-shark-2': theme === 'dark' },
																{ 'bg-color-mercury-2 text-color-bitter': theme === 'light' }
															)}
															type="textarea" 
															name="text" 
															id="exampleText" 
														/>
													</div>
												</Col>
											</Row>
											<Row>
												<Col sm={6}>
													<FormGroup >
													    <Link 
													        size="lg"
													        role="button"
													        className="btn btn-info w-50 btn-lg text-white"
													      >
													      Send
													    </Link>
													</FormGroup>
												</Col>

											</Row>
										</Form>


									</div>
								</div>

							</div>
						</Col>
					</Row>
				</div>
			</SidebarLayout>
		</div>

		

	);
}

export default Support;

