import React from 'react';
import ReactDOM from 'react-dom';

import {  BrowserRouter} from 'react-router-dom';


import throttle from 'lodash/throttle';
import './assets/base.scss';
import App from './layout/App';

import configureStore from './config/configureStore';
import { Provider } from 'react-redux';

import { saveState } from './config/localStorage';


const store = configureStore();
const rootElement = document.getElementById('root');

store.subscribe(throttle(() => {
  saveState({
    Assets: store.getState().Assets,
  });
}, 1000));


ReactDOM.render(
	<Provider store={store}>
		<BrowserRouter>
			<App />
		</BrowserRouter>
	</Provider>,
  rootElement
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
