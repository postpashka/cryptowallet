import { 
	SET_ASSETS,
  SET_USER_ASSETS_ORDER_NAME,
	GET_ASSETS_STARTED,
	GET_ASSETS_SUCCESS, 
	GET_ASSETS_FAILURE,
} from "./types";

import { SET_USER_ASSETS } from "./types";


import axios from "axios";



let defaultAssets = [
  {
    currency: 'BTC',
    price:  '$11',
    change: '+0.9%',
    balance: '0.0819132',
    value: '$651.49',
    portfolio: '10'
  },
  {
    currency: 'ETH',
    price:  '$2',
    change: '-0.8%',
    balance: '0.0819132',
    value: '$651.49',
    portfolio: '20'
  },
  {
    currency: 'DASH',
    price:  '$3',
    change: '+0.7%',
    balance: '0.0819132',
    value: '$651.49',
    portfolio: '30'
  },
  {
    currency: 'LTC',
    price:  '$4',
    change: '-0.6%',
    balance: '0.0819132',
    value: '$651.49',
    portfolio: '40'
  },

  {
    currency: 'USDT',
    price:  '$5',
    change: '+0.5%',
    balance: '0.0819132',
    value: '$651.49',
    portfolio: '50'
  },
  {
    currency: 'BNC',
    price:  '$6',
    change: '-0.4%',
    balance: '0.0819132',
    value: '$651.49',
    portfolio: '60'
  },
  {
    currency: 'XRP',
    price:  '$7',
    change: '+0.3%',
    balance: '0.0819132',
    value: '$651.49',
    portfolio: '70'
  },
  {
    currency: 'BSV',
    price:  '$8',
    change: '-0.2%',
    balance: '0.0819132',
    value: '$651.49',
    portfolio: '80'
  },
]





export const getAssets = () => {
  return (dispatch, state) => {
    dispatch(getAssetsStarted());

    axios
      .get(`https://jsonplaceholder.typicode.com/todos`, {
        title:'title',
        userId:'userId',
        completed: false
      })
      .then(res => {
      	setTimeout(() => {

          let responseData = defaultAssets;

          let assetsOrderKeys = state().Assets.assets.reduce((order, item) => {
            order.push(item.currency)
            return order;
          } , [])

          let userAssetsOrderKeys = state().Assets.userAssets.reduce((order, item) => {
            order.push(item.currency)
            return order;
          } , [])

          let sortedAssets = responseData.sort((a, b) =>{  
            return assetsOrderKeys.indexOf(a.currency) - assetsOrderKeys.indexOf(b.currency);
          }).filter(item => {
            return !userAssetsOrderKeys.includes(item.currency)
          });

	        dispatch(getAssetsSuccess( sortedAssets ));

      	}, 2500);
      })
      .catch(err => {
        dispatch(getAssetsFailure(err.message));
      });

  };
};

const getAssetsStarted = (assets) => ({
  type: GET_ASSETS_STARTED,
});

const getAssetsSuccess = assets => ({
  type: GET_ASSETS_SUCCESS,
  payload: {
    assets
  }
});

const getAssetsFailure = error => ({
  type: GET_ASSETS_FAILURE,
  payload: {
    error
  }
});


export const setAssets = (assets) => ({
  type: SET_ASSETS,
  payload:{
  	assets
  }
});

export const setUserAssets = (assets) => ({
  type: SET_USER_ASSETS,
  payload:{
  	assets
  }
});

export const setUserAssetsOrderName = (assetsOrder) => ({
  type: SET_USER_ASSETS_ORDER_NAME,
  payload:{
    assetsOrder
  }
});





export const setUserAssetsOrder = (orderBy) => {
  return (dispatch, state) => {

    if (orderBy !== 'default') {

      let sortedList = [...state().Assets.userAssets]

      sortedList.sort((a,b) => compare( a, b, orderBy ));

      dispatch(setUserAssets(sortedList))
    }

      dispatch(setUserAssetsOrderName(orderBy))


  };
};


function compare( a, b, value ) {

  // type - string or float 

  let ValueA = a[value];
  let ValueB = b[value];

  let type = ( value === 'currency' ) ? 'string' : 'float';

  if (ValueA.startsWith('$')){ValueA = ValueA.substring(1)};
  if (ValueB.startsWith('$')){ValueB = ValueB.substring(1)};

  if (type === 'float'){
    ValueA = parseFloat(ValueA);
    ValueB = parseFloat(ValueB);
  }

  if ( ValueA < ValueB ){
    return -1;
  }

  if ( ValueA > ValueB ){
    return 1;
  }
  return 0;
}