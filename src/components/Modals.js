import React, { useState } from "react";
import { useSelector } from "react-redux";
import {  Link } from 'react-router-dom';

import {
    Row,
    Label,
    Button,
    FormGroup,
    Col,
    ModalBody,
    Modal
} from 'reactstrap';
import cx from 'classnames';

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";


import {ReactComponent as CopyPaste} from '../assets/icons/copypaste.svg';

import {ReactComponent as CloseIcon} from '../assets/icons/close.svg';

import {ReactComponent as ArrowUp} from '../assets/icons/arrow-up.svg';

import qr from '../assets/imgs/qr.png';


import successIcon from '../assets/icons/modals/success.svg';
import loadingIcon from '../assets/icons/modals/loading.svg';




function ModalContainer(props) {
    //const dispatch = useDispatch();
    let params = new URLSearchParams(props.location.search);
    const toggleModalHandler = () => {
        //dispatch( clearMessage() );
        props.history.push(props.location.pathname);
    };
    return (
        <Modal 
            toggle={toggleModalHandler}
            //isOpen={ isForgotPasswordModalOpen }  
            isOpen={ !!params.get(props.modalName) }  
            className="modal-dialog modal-dialog-centered" 
            //backdrop="static" 
            //keyboard={false}
        >
            {props.children}
        </Modal>
    );
}







const SendModal = (props) => {

    const { theme } = useSelector(state => state.Theme);

    const [ activeItem, setActiveItem ] = useState( 1 );

    const [ sendTo, setSendTo ] = useState( '' );

    function toggleBtn(index) {
        if ( index === activeItem ) {
            setActiveItem(0)
        } else {
            setActiveItem(index)
        }
    }

    function copyPasteClick() {
        navigator.clipboard.readText().then((text) => {
            setSendTo(text)
        })
    }

  return (
    <ModalContainer {... props} modalName="send">
      <ModalBody 
        className={cx(
            "py-3 px-5",
            { 'bg-color-elephant-3 text-white ': theme === 'dark' },
            { 'bg-color-seashell': theme === 'light' }
        )}
    >
        <button 
            type="button" 
            className="close position-absolute ml-auto mt-3 mr-3 input-icon input-icon-right" 
            onClick={()=>props.history.push(props.location.pathname)}
        >
            <CloseIcon className="icon-white"/>
        </button>
        <div className="text-center">
            <img className="m-3" width="100px" height="100px" src={props.currency.img} alt="placeholder"/>
            <h4>Send {props.currency.name}</h4>
        </div>

        <Form 
            //onSubmit={handleLogin} 
            //ref={form} 
            className="w-100 my-3"
        >
            <FormGroup className="mb-4">
                <Label
                    className={cx(
                        "small py-1",
                        { 'text-color-white': theme === 'dark' },
                        { 'text-color-delta': theme === 'light' }
                    )}
                    for="sendTo"
                >
                    Send To
                </Label>
                <div className="position-relative">
                    <Input
                        type="text"
                        className={cx(
                            "form-control form-control-lg pr-5 rounded-lg",
                            { 'bg-color-nile-blue text-color-smalt-blue': theme === 'dark' },
                            { 'bg-color-white text-color-bitter': theme === 'light' }
                        )}
                        name="sendTo"
                        value={sendTo}
                        onChange={(e)=>setSendTo(e.target.value)}
                        placeholder="Enter Bitcoin address"
                    />
                    <div
                        className="input-icon input-icon-right position-absolute p-3"
                    >
                        <CopyPaste
                            style={{'cursor':'pointer'}}
                            onClick={copyPasteClick}
                            className={cx(
                                { 'icon-elm': theme === 'dark' },
                                { 'icon-delta': theme === 'light' }
                            )}
                        />
                    </div>
                </div>
            </FormGroup>
            <FormGroup className="mb-4">
                <Label
                    className={cx(
                        "small py-1",
                        { 'text-color-white': theme === 'dark' },
                        { 'text-color-delta': theme === 'light' }
                    )}
                    for="note"
                >
                    Personal Note (Optional)
                </Label>
                <div className="position-relative">
                    <Input
                        type="text"
                        className={cx(
                            "form-control form-control-lg rounded-lg",
                            { 'bg-color-nile-blue text-color-smalt-blue': theme === 'dark' },
                            { 'bg-color-white text-color-bitter': theme === 'light' }
                        )}
                        name="note"
                        placeholder="Enter Personal Note"
                    />
                </div>
            </FormGroup>
            <FormGroup className="mb-4">
                <Label
                    className={cx(
                        "small py-1",
                        { 'text-color-white': theme === 'dark' },
                        { 'text-color-delta': theme === 'light' }
                    )}
                    for="amount"
                >
                    Auto Amount
                </Label>
                <Row className="mx-n1">
                    <Col sm={4} className="px-1">
                        <Button
                            outline
                            color="info"
                            onClick={()=>toggleBtn(1)}
                            className={cx(
                                'btn-block',
                                { 'btn-info text-white': activeItem === 1 }
                            )}
                            //color="info"
                        >
                        MAX
                      </Button>
                    </Col>
                    <Col sm={4} className="px-1">
                        <Button
                            outline
                            color="info"
                            onClick={()=>toggleBtn(2)}
                            className={cx(
                                'btn-block',
                                { 'btn-info text-white': activeItem === 2 }
                            )}
                            //outline
                            //color="info"
                        >
                        HALF
                      </Button>
                    </Col>
                    <Col sm={4} className="px-1">
                        <Button
                            outline
                            color="info"
                            onClick={()=>toggleBtn(3)}
                            className={cx(
                                'btn-block',
                                { 'btn-info text-white': activeItem === 3 }
                            )}
                        >
                        MIN
                      </Button>
                    </Col>
                </Row>
            </FormGroup>
            <Row className="mx-n2">
              <Col md={6} className="px-2">
                <FormGroup>
                  <Label className="small" for="BTC">BTC</Label>
                  <Input 
                    className={cx(
                        "form-control form-control-lg rounded-lg",
                        { 'bg-color-nile-blue text-color-smalt-blue': theme === 'dark' },
                        { 'bg-color-white text-color-bitter': theme === 'light' }
                    )}
                    type="text"
                    name="BTC"
                    id="BTC"
                    placeholder="Enter amount in BTC"
                    />
                </FormGroup>
              </Col>
              <Col md={6} className="px-2">
                <FormGroup>
                  <Label className="small" for="USD">USD</Label>
                  <Input 
                    className={cx(
                        "form-control form-control-lg rounded-lg",
                        { 'bg-color-nile-blue text-color-smalt-blue': theme === 'dark' },
                        { 'bg-color-white text-color-bitter': theme === 'light' }
                    )}
                    type="text"
                    name="BTC"
                    id="USD"
                    placeholder="Enter amount in USD"
                    />
                </FormGroup>
              </Col>
            </Row>
            <FormGroup 
                className={cx(
                    "mb-4 p-3",
                    { 'bg-color-black-pearl-2 text-color-oslo-gray-2': theme === 'dark' },
                    { 'bg-color-white text-color-bitter': theme === 'light' }
                )}
            >
                <Row className="mb-3">
                    <Col md={5}>
                        <span>Transaction Fee:</span>
                    </Col>
                    <Col md={7} className="text-right">
                        <span className="px-1">0.00 BTC</span>
                        <span className="px-1">0.00 USD</span>
                    </Col>
                </Row>
                <Row>
                    <Col md={5}>
                        <span>Available:</span>
                    </Col>
                    <Col md={7} className="text-right">
                        <span className="px-1">0.00 BTC</span>
                        <span className="px-1">0.00 USD</span>
                    </Col>
                </Row>
            </FormGroup>
            <FormGroup className="mb-4">
                <Link 
                    size="lg"
                    role="button"
                    className="btn btn-block btn-primary"
                    to={{ 
                        search: "?send-success=true" 
                    }}
                  >
                  <ArrowUp className="mr-2"/>
                  Send
                </Link>
                <Link 
                    size="lg"
                    role="button"
                    className="btn btn-block btn-primary"
                    to={{ 
                        search: "?send-error=true" 
                    }}
                  >
                  <ArrowUp className="mr-2"/>
                  Send
                </Link>
                <Link 
                    size="lg"
                    role="button"
                    className="btn btn-block btn-primary"
                    to={{ 
                        search: "?send-loading=true" 
                    }}
                  >
                  <ArrowUp className="mr-2"/>
                  Send
                </Link>
            </FormGroup>
        </Form>
      </ModalBody>
    </ModalContainer>
  );
};



const ReceiveModal = (props) => {

    const { theme } = useSelector(state => state.Theme);

    const [ reciveBy, setReciveBy ] = useState( '' );


    function copyPasteClick() {
        navigator.clipboard.readText().then((text) => {
            setReciveBy(text)
        })
    }

    return (
    <ModalContainer {... props} modalName="receive">
        <ModalBody
            className={cx(
                "py-3 px-5",
                { 'bg-color-elephant-3 text-white ': theme === 'dark' },
                { 'bg-color-seashell': theme === 'light' }
            )}
        >
            <button 
                type="button" 
                className="close position-absolute ml-auto mt-3 mr-3 input-icon input-icon-right" 
                onClick={()=>props.history.push(props.location.pathname)}
            >
                <CloseIcon className="icon-white"/>
            </button>

            <div className="text-center">
                <img className="m-3" width="100px" height="100px" src={props.currency.img} alt="placeholder"/>
                <h4>Receive {props.currency.name}</h4>
            </div>
            <div className="text-center my-5">
                <div className="bg-white p-1 rounded-lg d-inline-block">
                    <img className="m-3" width="200px" height="200px" src={qr} alt="placeholder"/>
                </div>
                <div className="py-3">
                    <Link > Use the address below to receive funds.</Link>
                </div>
            </div>
            <Form>
                <FormGroup className="mb-4">
                    <Label
                        className={cx(
                            "small py-1",
                            { 'text-color-white': theme === 'dark' },
                            { 'text-color-delta': theme === 'light' }
                        )}
                        for="note"
                    >
                        Your Bitcoin Address
                    </Label>
                    <div className="position-relative">
                        <Input
                            type="text"
                            className={cx(
                                "form-control form-control-lg rounded-lg border-dashed",
                                { 'bg-color-elephant-3 text-color-smalt-blue': theme === 'dark' },
                                { 'bg-color-white text-color-bitter': theme === 'light' }
                            )}
                            name="note"
                            placeholder="bc1qv72zvy3ev890sr8hht5635mzk0lttzzfwg8z0t"
                            value={reciveBy}
                            onChange={(e)=>setReciveBy(e.target.value)}
                        />
                        <div
                            className="input-icon input-icon-right position-absolute p-3"
                        >
                            <CopyPaste
                                style={{'cursor':'pointer'}}
                                onClick={copyPasteClick}
                                className={cx(
                                    { 'icon-elm': theme === 'dark' },
                                    { 'icon-delta': theme === 'light' }
                                )}
                            />
                        </div>
                    </div>
                </FormGroup>
                <FormGroup className="mb-4">
                    <Button
                        size="lg"
                        color="info"
                        className="btn-block"
                    >
                        <span className="text-white">
                        Send Via Email
                        </span>
                    </Button>
                </FormGroup>
            </Form>
      </ModalBody>
    </ModalContainer>
  );
};





const SuccessModal = (props) => {

    const { theme } = useSelector(state => state.Theme);

    return (
        <ModalContainer {... props} modalName="send-success">
            <ModalBody
                className="px-0 pb-0 pt-2 bg-success rounded-lg"
            >

                <button 
                    type="button" 
                    className="close position-absolute ml-auto mt-3 mr-3 input-icon input-icon-right" 
                    onClick={()=>props.history.push(props.location.pathname)}
                >
                    <CloseIcon className="icon-success"/>
                </button>

                <div
                    className={cx(
                        "py-3 px-5 rounded-lg",
                        { 'bg-color-elephant-3 text-white ': theme === 'dark' },
                        { 'bg-color-seashell': theme === 'light' }
                    )}
                >

                    <div 
                        className="d-flex tall-modal align-items-center justify-content-center"
                    >
                        <div>
                            <div className="text-center pb-5">
                                <img className="m-3" width="100px" height="100px" src={successIcon} alt="placeholder"/>
                                <h4 className="font-weight-bold">2671.53 USD</h4>
                            </div>
                            <div className="text-center pt-5">
                                <h5>Will be Send to:</h5>
                                <p className="text-color-blue-bayoux py-2">16azaQHPThoQcf14o2XqmbG</p>
                            </div>
                        </div>
                    </div>
                </div>



          </ModalBody>
        </ModalContainer>
  );
};





const LoadingModal = (props) => {

    const { theme } = useSelector(state => state.Theme);

    return (
        <ModalContainer {... props} modalName="send-error">
            <ModalBody className="p-0">

                <div
                    className={cx(
                        "py-3 px-5 rounded-lg",
                        { 'bg-color-elephant-3 text-white ': theme === 'dark' },
                        { 'bg-color-seashell': theme === 'light' }
                    )}
                >

                    <div 
                        className="d-flex tall-modal align-items-center justify-content-center"
                    >
                        <div>
                            <div className="text-center pb-5">
                                <img className="m-3 rotated-img" width="100px" height="100px" src={loadingIcon} alt="placeholder"/>
                                <h5 className="font-weight-bold">Loading</h5>
                            </div>
                        </div>
                    </div>
                </div>



          </ModalBody>
        </ModalContainer>
  );
};




const ErrorModal = (props) => {

    const { theme } = useSelector(state => state.Theme);

    return (
        <ModalContainer {... props} modalName="send-loading">
            <ModalBody
                className="px-0 pb-0 pt-2 bg-danger rounded-lg"
            >

                <div
                    className={cx(
                        "py-3 px-5 rounded-lg",
                        { 'bg-color-elephant-3 text-white ': theme === 'dark' },
                        { 'bg-color-seashell': theme === 'light' }
                    )}
                >

                    <div 
                        className="d-flex tall-modal align-items-center justify-content-center"
                    >
                        <div>
                            <div 
                                className="text-center pb-5"
                            >
                                <h1
                                    className="pb-3 text-danger"
                                >
                                    Error
                                </h1>
                                <h5 className="font-weight-bold">Inavild Amount</h5>
                            </div>
                        </div>
                    </div>
                </div>



          </ModalBody>
        </ModalContainer>
  );
};











export {
    SendModal,
    ReceiveModal,
    SuccessModal,
    ErrorModal,
    LoadingModal,
}