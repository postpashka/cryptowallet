import React from "react";
import { useDispatch, useSelector } from "react-redux";



import { toggleTheme } from "../actions/theme";

import Switch from "react-switch";

const ThemeToggle = (props) => {


	const { theme } = useSelector(state => state.Theme);

	const dispatch = useDispatch();

	const toggleThemeHandler = (e) => {
		dispatch(toggleTheme())
	};

	return (
		<div
			style={{
				position: 'fixed',
				zIndex: '99',
				bottom: '20px',
				right: '50px'

			}}
		>
			<Switch
				onChange={toggleThemeHandler}
				checked={ theme === 'dark'} 

				onColor="#86d3ff"
				onHandleColor="#2693e6"
				handleDiameter={30}
				uncheckedIcon={false}
				checkedIcon={false}
				boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
				activeBoxShadow={ theme === 'dark' ? "0px 0px 1px 10px rgba(255, 255, 255, 0.2)" : "0px 0px 1px 10px rgba(0, 0, 0, 0.2)"}  
				height={20}
				width={48}
				className="react-switch"
				id="material-switch"

			/>
		</div>
	);
}

export default ThemeToggle;

