import btc from '../assets/icons/currency/btc.svg';
import eth from '../assets/icons/currency/eth.svg';
import dash from '../assets/icons/currency/dash.svg';
import ltc from '../assets/icons/currency/ltc.svg';
import usdt from '../assets/icons/currency/usdt.svg';
import bnc from '../assets/icons/currency/bnc.svg';
import xrp from '../assets/icons/currency/xrp.svg';
import bsv from '../assets/icons/currency/bsv.svg';


let currencyList = {
	'BTC': {
		name: 'Bitcoin',
		img: btc,
		color: '#F7931A'
	},
	'ETH': {
		name: 'Ethereum',
		img: eth,
		color: '#8C8C8C'
	},
	'DASH': {
		name: 'Dash',
		img: dash,
		color: '#4C7BF6'
	},
	'LTC': {
		name: 'Litecoin',
		img: ltc,
		color: '#345D9D'
	},

	'USDT': {
		name: 'Tether USD',
		img: usdt,
		color: '#50AF95'
	},
	'BNC': {
		name: 'Binance Coin',
		img: bnc,
		color: '#F3BA2F'
	},
	'XRP': {
		name: 'XRP',
		img: xrp,
		color: '#FFFFFF'
	},
	'BSV': {
		name: 'Bitcoin SV',
		img: bsv,
		color: '#EAB300'
	},
}



export default currencyList;