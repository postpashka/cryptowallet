import React, { PureComponent } from 'react';
import { AreaChart, Area, XAxis, YAxis, Tooltip } from 'recharts';

const data = [
  {
    name: 'Page A',
    uv: 4000,
    pv: 2400,
    amt: 2400,
  },
  {
    name: 'Page B',
    uv: 3000,
    pv: 1398,
    amt: 2210,
  },
  {
    name: 'Page C',
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
  {
    name: 'Page D',
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: 'Page E',
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: 'Page F',
    uv: 2390,
    pv: 3800,
    amt: 2500,
  },
  {
    name: 'Page G',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
];

class WalletChart extends PureComponent {
  render() {
    return (

    	<div>
    		<div className="d-flex mt-4 mb-3 small">
    			<span>Price(30D)</span>
    			<span className="ml-auto text-success">1BTC = $7,296.46 </span>
    		</div>
	      <div className="d-flex justify-content-center">
	        <AreaChart
	          width={330}
	          height={180}
	          data={data}
	          margin={{
	            top: 10,
	            right: 0,
	            left: -50,
	            bottom: -50,
	          }}
	        >
	        <defs>
	            <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
	                <stop offset="5%" stopColor="#386FF9" stopOpacity={1}/>
	                <stop offset="95%" stopColor="#386FF9" stopOpacity={0.1}/>
	            </linearGradient>
	        </defs>

	          <XAxis dataKey="name" tick={false}/>
	          <YAxis tick={false}/>
	          <Tooltip />
	          <Area  dataKey="uv" stroke="#8884d8" fill="url(#colorUv)" />
	        </AreaChart>
	      </div>
	      <div className="d-flex ">
	      	<span className="text-success">+1.29%</span>
	      </div>

    	</div>
    );
  }
}

export default WalletChart;

