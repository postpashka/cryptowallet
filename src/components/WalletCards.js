import React, { useState } from "react";

import { useDispatch, useSelector } from "react-redux";


import cx from 'classnames';


import {
		Col, 
		Row,
} from 'reactstrap';

import { setAssets, setUserAssets } from "../actions/assets";



import {ReactComponent as ThreeDots} from '../assets/icons/three-dots.svg';
import {ReactComponent as ThreeLines} from '../assets/icons/three-lines.svg';

import currencyList from '../components/CurrencyList';



const WalletCard = (props) => {

	const { theme } = useSelector(state => state.Theme);


	const [ isIconHovered, setIsIconHovered ] = useState( false );


	const handleMouseOver = () => {
		setIsIconHovered(true)
	};

	const handleMouseOut = () => {
		setIsIconHovered(false)
	};

	return (
		<div className="mt-1 mb-3" 
			onDragStart={props.onDragStart}
			onDragEnter={props.onDragEnter}
			onClick={props.onClick}
			draggable={isIconHovered}
		>
			<div 
				className={cx(
					"wallet-card text-center p-3 rounded-lg",
					{ 'bg-color-firefly-2': theme === 'dark' },
					{ 'bg-white': theme === 'light' }
				)}
			>
				<div className="d-flex flex-row p-1">

					<div className="d-flex align-items-center pr-3">
						<ThreeDots
							className="grabbable"
							onMouseEnter={handleMouseOver}
							onMouseLeave={handleMouseOut}
						/>
					</div>
					<div className="d-flex flex-grow-1">
						<Row className="text-center d-flex flex-grow-1">	
							<Col sm={2} className="d-flex align-items-center flex-row">


								<div className="d-flex flex-column align-items-center justify-content-center flex-grow-1">

									<img 
										className="mt-1 mb-2"
										src={currencyList[props.item.currency].img}
										alt="placeholder"
									/>
									<div
										style={{ 'color' : currencyList[props.item.currency].color}}
									>
										{currencyList[props.item.currency].name}
									</div>
									<div className="text-gray-600 smaller">
										{props.item.currency}
									</div>
								</div>
							</Col>
							<Col sm={2} className="d-flex align-items-center justify-content-center flex-column p-0">
								<div className="border-left border-dark w-100 px-3">
									<div
										className="text-gray-600 smaller text-uppercase mb-3"
									>
										price
									</div>
									{props.item.price}
								</div>
							</Col>
							<Col sm={2} className="d-flex align-items-center justify-content-center flex-column">
								<div
									className="text-gray-600 smaller text-uppercase mb-3"
								>
									24h change
								</div>
								<div
									className={cx(
										{ 'text-danger': props.item.change[0] === '-' },
										{ 'text-success': props.item.change[0] === '+' },
									)}
								>
									{props.item.change}
								</div>
							</Col>
							<Col sm={2} className="d-flex align-items-center justify-content-center flex-column">
								<div
									className="text-gray-600 smaller text-uppercase mb-3"
								>
									your balance
								</div>
								<div
									style={{ 'color' : currencyList[props.item.currency].color}}
								>
									{props.item.balance} {props.item.currency}
								</div>
							</Col>
							<Col sm={2} className="d-flex align-items-center justify-content-center flex-column">
								<div
									className="text-gray-600 smaller text-uppercase mb-3"
								>value</div>
								{props.item.value}
							</Col>
							<Col sm={2} className="d-flex align-items-center justify-content-center flex-column">
								<div
									className="text-gray-600 smaller text-uppercase mb-3"
								>portfolio</div>
								{props.item.portfolio}
							</Col>
						</Row>
					</div>
					<div className="d-flex align-items-top pl-3">
						<ThreeLines/>
					</div>
				</div>

			</div>
		</div>
	);
}

const WalletCardSmall = (props) => {

	const { theme } = useSelector(state => state.Theme);

	const [ isIconHovered, setIsIconHovered ] = useState( false );

	const { userAssets } = useSelector(state => state.Assets);
	const { assets } = useSelector(state => state.Assets);

	const dispatch = useDispatch();


	const handleMouseOver = () => {
		setIsIconHovered(true)
	};

	const handleMouseOut = () => {
		setIsIconHovered(false)
	};


	const exploreAssetsClick = (item) => {

		const updatedUserAssetsList = [...userAssets];
		const updatedAssetsList = [...assets];

		updatedAssetsList.splice( updatedAssetsList.indexOf(item) , 1);

		updatedUserAssetsList.push(item)

		dispatch(setUserAssets(updatedUserAssetsList))
		dispatch(setAssets(updatedAssetsList))
	};

	return (
		<div 
			draggable={isIconHovered}
			onDragStart={props.onDragStart}
			onDragEnter={props.onDragEnter}
			className={cx(
				"wallet-card my-2 text-center px-4 py-2 rounded-lg",
				{ 'bg-blue-700': theme === 'dark' },
				{ 'bg-white': theme === 'light' }
			)}
		>
			<div className="d-flex flex-row p-1">
				<div className="d-flex align-items-center pr-3">
					<ThreeDots 
						width="10px" 
						height="18px"
						className="grabbable"
						onMouseEnter={handleMouseOver}
						onMouseLeave={handleMouseOut}
					/>
				</div>
				<Row className="text-center d-flex flex-grow-1">	
					<Col sm={6} className="d-flex align-items-center justify-content-center flex-row pr-0">

						<div className="d-flex flex-column align-items-center justify-content-center flex-grow-1">


							<img width="30px" height="30px" src={ currencyList[props.item.currency].img } alt="placeholder"/>
							<div
								style={{ 'color' : currencyList[props.item.currency].color}}
							>
								{currencyList[props.item.currency].name}
							</div>
							<div
								className="smaller"
							>
								{props.item.currency}
							</div>
						</div>
					</Col>
					<Col sm={6} className="d-flex align-items-center justify-content-center flex-column">
						<div className="border-left border-dark w-100">
							<div>{props.item.price}</div>
							<div
								className={cx(
									'small',
									{ 'text-danger': props.item.change[0] === '-' },
									{ 'text-success': props.item.change[0] === '+' },
								)}
							>
								{props.item.change}
							</div>
						</div>
					</Col>
				</Row>
				<div 
					className="d-flex align-items-top pl-3"
					onClick={()=>exploreAssetsClick(props.item)}
				>
					+
				</div>
			</div>
		</div>
	);
}

export {
	WalletCard,
	WalletCardSmall
};

