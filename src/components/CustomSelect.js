import React from "react";
import { useSelector } from "react-redux";
import Select from "react-select";


const customStyles = {
	option: (provided, state) => ({
		...provided,
		padding: '8px 16px',
		backgroundColor: 'transparent',
	}),
	control: (provided, state) => ({
		height: 60,
		display: 'flex',
		border: 'transparent',
		borderRadius: '10px',
		//backgroundColor: '#0E2D37',
		backgroundColor:  state.selectProps.theme === 'dark' ? '#0E2D37' : '#E9E9E9',
	}),
/*	valueContainer: () => ({
		color: '#86969b',
		alignItems: 'center',
		display: 'flex',
		flex: '1',
		flexWrap: 'wrap',
		padding: '2px 8px',
		position: 'relative',
		overflow: 'hidden',
		boxSizing: 'border-box',
	}),	*/
	singleValue: () => ({
		color: '#86969b',
		paddingLeft: '16px',
	}),
	indicatorSeparator: () => ({
		display: 'none',
		//backgroundColor: 'pink',
	}),

	menu: (provided, state) => ({
		color: '#86969b',
		position: 'absolute',
		width: '100%',
		backgroundColor:  state.selectProps.theme === 'dark' ? '#08232C' : '#cfcfcf',
		zIndex: '10'
	}),

/*	singleValue: (provided, state) => {
		const opacity = state.isDisabled ? 0.5 : 1;
		const transition = 'opacity 300ms';

		return { ...provided, opacity, transition };
	}*/
}


const CustomSelect = (props) => {

	const { theme } = useSelector(state => state.Theme);

	return (
		<Select
			theme={theme}
			styles={customStyles}
			defaultValue={props.options[0]}
			options={props.options}
		/>
	);
}




export default CustomSelect;

