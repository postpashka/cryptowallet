import React from 'react';

import { isEmail } from 'validator';

const required = (value, props) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        Поле обязательно
      </div>
    );
  }
};

const emailVal = (value) => {
  if (!isEmail(value)) {
    return (
      <div className="alert alert-danger" role="alert">
        {value} неверный формат
      </div>
    );
  }
};

const isEqual = (value, props, components) => {
  const bothUsed = components.password[0].isUsed && components.confirmPassword[0].isUsed;
  const bothChanged = components.password[0].isChanged && components.confirmPassword[0].isChanged;

  if (bothChanged && bothUsed && components.password[0].value !== components.confirmPassword[0].value) {
    return (
      <div className="alert alert-danger" role="alert">
        Пароли не совпадают
      </div>
    );
  }
};




export {
    required,
    emailVal,
    isEqual,
}