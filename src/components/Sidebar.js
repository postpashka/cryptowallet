import React from "react";

import { useDispatch, useSelector } from "react-redux";

import { Link } from "react-router-dom";


import {
		Nav,
		NavItem,
		Media
} from 'reactstrap';

import cx from 'classnames';

import logo from '../assets/imgs/logo.png';

import { authActions } from "../actions/auth";


import {ReactComponent as HomeIcon} from '../assets/icons/nav/home.svg';
import {ReactComponent as WalletIcon} from '../assets/icons/nav/wallet.svg';
import {ReactComponent as ExchangeIcon} from '../assets/icons/nav/exchange.svg';
import {ReactComponent as TransactionsIcon} from '../assets/icons/nav/transactions.svg';
import {ReactComponent as SettingsIcon} from '../assets/icons/nav/settings.svg';
import {ReactComponent as SupportIcon} from '../assets/icons/nav/support.svg';
import {ReactComponent as SignoutIcon} from '../assets/icons/nav/signout.svg';

import {ReactComponent as CoinsIcon} from '../assets/icons/nav/coins.svg';




const Sidebar = (props) => {


	const dispatch = useDispatch();

	const { theme } = useSelector(state => state.Theme);


	function logOutHandler(){
		dispatch( authActions.logout() )
	}

	let {
	    match
	} = props;
	return (
		<div 
			className={cx(
				"sidebar h-100",
				{ 'bg-color-elephant': theme === 'dark' },
				{ 'bg-white': theme === 'light' },
			)}
		>

			<div className="sidebar-container d-flex flex-grow-1 flex-column h-100"> 

				<div className="sidebar-top flex-grow-1 overflow-hidden"> 

					<Link to="/" className="logo p-5 d-block">
						<img src={logo} className="mx-auto d-block mw-100" alt="placeholder"/>
					</Link>
					<div className="navs">
						<Nav vertical>
							<NavItem className="px-3 py-2 small my-2">
								<span className="px-2 ml-4 pl-1">
								MENU
								</span>
							</NavItem>
							<NavItem className="my-2">
								<Link 
									to="/home" 
									className={cx(
										"d-flex align-items-center px-3 py-2",
										{ 'active': match.path === '/home' },
									)}
								>
									<HomeIcon className="mr-3 ml-4"/>
									Home
								</Link>
							</NavItem>
							<NavItem className="my-2">
								<Link 
									to="/wallet" 
									className={cx(
										"d-flex align-items-center px-3 py-2",
										{ 'active': match.path === '/wallet' },
									)}
								>
									<WalletIcon className="mr-3 ml-4"/>
									Wallet
								</Link>
							</NavItem>
							<NavItem className="my-2">
								<Link 
									to="/exchange" 
									className={cx(
										"d-flex align-items-center px-3 py-2",
										{ 'active': match.path === '/exchange' },
									)}
								>
									<ExchangeIcon className="mr-3 ml-4"/>
									Exchange
								</Link>
							</NavItem>
							<NavItem className="my-2">
								<Link 
									to="/transactions" 
									className={cx(
										"d-flex align-items-center px-3 py-2",
										{ 'active': match.path === '/transactions' },
									)}
								>
									<TransactionsIcon className="mr-3 ml-4"/>
									Transactions
								</Link>
							</NavItem>
						</Nav>
						<Nav vertical>
							<NavItem className="px-3 py-2 my-3 small">
								<span className="px-2 ml-4 pl-1">
								ACCOUNT
								</span>
							</NavItem>
							<NavItem className="my-2">
								<Link 
									to="/settings" 
									className={cx(
										"d-flex align-items-center px-3 py-2",
										{ 'active': match.path === '/settings' },
									)}
								>
									<SettingsIcon className="mr-3 ml-4"/>
									Settings
								</Link>
							</NavItem>
							<NavItem className="my-2">
								<Link 
									to="/support" 
									className={cx(
										"d-flex align-items-center px-3 py-2",
										{ 'active': match.path === '/support' },
									)}
								>
									<SupportIcon className="mr-3 ml-4"/>
									Support
								</Link>
							</NavItem>
							<NavItem className="my-2">

								<Link 
									to="#" 
									className={cx(
										"d-flex align-items-center px-3 py-2",
										{ 'active': match.path === '/login' },
									)}
									onClick={logOutHandler}
								>
									<SignoutIcon className="mr-3 ml-4"/>
									SignOut
								</Link>
							</NavItem>
						</Nav>
					</div>

				</div>
				<div 
					className="sidebar-bottom mw-100 px-4 py-2"
				> 
					<Media className="px-3">
					  <Media 
					  	body
					  	className="px-1 h6"
					  >
					    <Media bottom className="mb-2">
					      <span className="h4">$2345.06</span>
					      <span className="px-3">USD</span>
					    </Media>
					    Your Portfolio's Worth
					  </Media>
					  <CoinsIcon 
					  	className="d-flex align-self-center"
					  />
					</Media>
				</div>

			</div>


		</div>
	);
}

export default Sidebar;

