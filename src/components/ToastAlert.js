import React, { useEffect, useState }  from 'react';

import { useSpring, useTransition, config, animated } from "react-spring";


import { useDispatch, useSelector } from "react-redux";

import { Toast, ToastBody, ToastHeader } from 'reactstrap';

import { alertActions } from '../actions/alert';

import { history } from '../helpers';


const ToastAlert = (props) => {


    const alert = useSelector(state => state.Alert);

    const [showAlert, setShowAlert] = useState(false);

    const dispatch = useDispatch();

    const clearToast = () => {

    	setShowAlert(false)
    	dispatch(alertActions.clear());;
    }

    useEffect(() => {
        history.listen((location, action) => {
            // clear alert on location change
            clearToast()
        });
    }, []);


/*    useEffect(() => {
    	debugger
    	console.log(alert)
    }, [alert]);
*/

    const fadeStyles = useSpring({
      config: { ...config.stiff },
      from: { 
      	opacity: 0,
      	position: "absolute",
      	bottom: "0px", 
      	right: "0", 
      	zIndex: "0",
        margin: "20px"
      },
      to: {
        opacity: alert.message ? 1 : 0,
        zIndex: alert.message ? '99' : '0',
      }
    });


    return (
        <animated.div 
        	className={`toast w-100 ${alert.type}`}
        	style={fadeStyles}
        >
        	<div className="d-flex toast-header w-100">
        	  <button 
        	    type="button" 
        	    class="ml-auto close" 
        	    data-dismiss="toast" 
        	    aria-label="Close"
        	    onClick={clearToast}
        	    >
        	      <span aria-hidden="true">&times;</span>
        	  </button>
        	</div>
        	<ToastBody>
        	  {alert.message}
        	</ToastBody>
        </animated.div>
    )
};
export default ToastAlert;
