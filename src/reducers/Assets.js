import { 
  SET_ASSETS,
  SET_USER_ASSETS,
  GET_ASSETS_SUCCESS,
  GET_ASSETS_FAILURE,
  GET_ASSETS_STARTED,
  SET_USER_ASSETS_ORDER_NAME
} from "../actions/types";


const initialState = {
  loading: false,
  error: null,
  assets: [],
	userAssets: [],
  assetsOrderName: '',

};

export default function assetsReducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {


    case GET_ASSETS_STARTED:
      return {
        ...state,
        loading: true
      };
    case GET_ASSETS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        assets: payload.assets
      };
    case GET_ASSETS_FAILURE:
      return {
        ...state,
        loading: false,
        error: payload.error
      };
    case SET_ASSETS:
      return {
          ...state,
          assets: payload.assets
      };
    case SET_USER_ASSETS:
      return {
          ...state,
          userAssets: payload.assets
      };
    case SET_USER_ASSETS_ORDER_NAME:
      return {
          ...state,
          assetsOrderName: payload.assetsOrderName
      };

    default:
      return state;
  }
}