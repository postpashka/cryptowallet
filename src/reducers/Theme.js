import { TOGGLE_THEME } from "../actions/types";

const initialState = {
	theme: 'dark'
};

export default function foo(state = initialState, action) {
  const { type } = action;

  switch (type) {
    case TOGGLE_THEME:
      return { theme: (state.theme === 'dark' ? 'light' : 'dark') };

    default:
      return state;
  }
}