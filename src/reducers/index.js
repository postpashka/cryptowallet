import Alert from './Alert';
import Message from './Message';
import Theme from './Theme';
import Auth from './Auth';
import Assets from './Assets';

import Registration from './Registration';


const reducers = {
    Message,
    Theme,
    Auth,
    Assets,
    Registration,
    Alert
};

export default reducers